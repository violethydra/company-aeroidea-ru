$startLogo = @'
-------------------------------
- Remove template files
-------------------------------
'@;

# Params
$MyFolders = 'node_modules', 'public';
$MyFiles = 'yarn.lock', 'package-lock.json', 'yarn-error.log';
$MySingleTmp = 'development/tmp';


$startLogo;
# Actions
Remove-Item -Path $MyFolders -Recurse -ErrorAction SilentlyContinue; 
Write-Output "Deleted: $($MyFolders.Parent)\$($MyFolders.Name)";

Remove-Item -Path $MyFiles -Recurse -ErrorAction SilentlyContinue; 
Write-Output "Deleted: $($MyFiles.Parent)\$($MyFiles.Name)";

Remove-Item -Path $MySingleTmp -Recurse -ErrorAction SilentlyContinue; 
Write-Output "Deleted: $($MySingleTmp.Parent)\$($MySingleTmp.Name)";

# End
@'
-------------------------------
- DONE!!!
-------------------------------
'@;

# Exit to any button
# Write-Host -NoNewLine 'Press any key to exit...';
# $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');

$collectName = 'Auto-Close after:';
$collection = '3s', '2s', '1s';

forEach ($item in $collection) {
  Clear-Host;
  Write-Host $startLogo;
  Write-Host $collectName, $item;
  Start-Sleep 1;
}

Exit;

