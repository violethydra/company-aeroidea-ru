// ============================
//    Name: index.js
// ============================

import $cardController from './modules/card/cardController';

const startPackage = (option) => {
	console.log('[DOM]:', 'DOMContentLoaded', option);

	new $cardController({
		prefix: 'stock',
		selector: '[data-js=controller]',
		fetchURL: './my-server.json',
		// fetchURL: 'https://my-json-server.typicode.com/aero-frontend/test-task/db',
		selectorJS: {
			rating: 'data-js=rating',
			viewPicture: 'data-js=viewPicture',
			inStock: 'data-js=inStock',
			listProperty: 'data-js=listProperty',
			favorites: 'data-js=favorites'
		},
		ratingStars: {
			dataAttr: 'data-rating',
			countStars: 5,
			activeStar: 3
		},
		viewPicture: {
			productPrefix: './media/img/',
			productUrl: ''
		},
		favorites: {
			fetchURL: 'https://jsonplaceholder.typicode.com/posts'
			// fetchURL: 'https://jsonplaceholder.typicode.com/posts/1'
		}
		
	}).run();

};

const start = (option) => {
	if (option === true) startPackage(option);
	else console.error('System: ', 'I think shit happens 😥 ');
};

const addCss = (fileName) => {
	const link = document.createElement('link');
	link.type = 'text/css';
	link.rel = 'stylesheet';
	link.href = fileName;

	document.head.appendChild(link);
};

if (typeof window !== 'undefined' && window && window.addEventListener) {

	if (navigator.userAgent.includes('Firefox')) addCss('css/firefox.css');
	if (navigator.userAgent.includes('Edge')) addCss('css/edge.css');

	document.addEventListener('DOMContentLoaded', start(true), false);
}
