export default class $addon {
	constructor(init) {
		// --- VARIABLES ---
		this.param1 = init._param1;
		this.param2 = init._param2;
		this.prefix = init.prefix;
		// ---
		this.main();
	}

	main() {
		console.log(this.param1, this.param2);
	}
}
