export default class $index {
	constructor(init) {
		this.selector = document.querySelector(init.selector);
		this.selectorID = init.selector;
		if (!this.selector) return;
		
		// --- VARIABLES ---
		__text__
		// ---
	}

	run() {
		if (this.selector) {
			this.firstRun();
			this.eventHandlers();
		}
	}

	eventHandlers() {
		// this.selectorParent.addEventListener('click', event => this.handlerClick(event), false);
	}

	firstRun() {
		this.main();
	}

	main() {
		__text__
	}
}
