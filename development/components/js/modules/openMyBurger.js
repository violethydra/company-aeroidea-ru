export default class $openMyBurger {
	constructor(init) {
		this.selector = document.querySelector(init.burger);
		if (!this.selector) return;

		this.prefix = init.prefix;
		this.navbar = document.querySelector(init.navbar);
		this.errorText = '💀 Ouch, database error...';
	}

	static info() { console.log('[MODULE]:', this.name, true); }

	run() {		
		if (this.selector) {
			this.constructor.info();
			this.firstRun();
			this.eventHandlers();
		}
	}

	eventHandlers() {
		this.selector.addEventListener('click', event => this.handlearOpenBurger(event), false);
		window.addEventListener('keydown', event => this.handlearOpenBurgerESC(event), false);
	}
	
	firstRun() {
		this.createMobileLinks();
	}
	
	handlearOpenBurgerESC(event) {
		if (this.navbar.classList.contains('open')) {
			if (event.key === 'Escape') this.navbar.classList.remove('open');
		}
	}
	
	handlearOpenBurger(event) {
		if (this.navbar.classList.contains('open')) this.navbar.classList.remove('open');
		else this.navbar.classList.add('open');
	}

	createMobileLinks() {
		const link = document.querySelectorAll('.js__headerText');
		const linkFirst = document.querySelectorAll('.js__headerText')[0].childElementCount || 0;
		
		const resultMap = (index) => {
			this.ar = [...link[index].children].map(g => ({
				name: g.textContent || '',
				url: g.attributes.href.textContent || ''
			}));
			return this.covnertArr.push(this.ar);
		};
		
		this.covnertArr = [];
		this.newArr = [...link].map((f, index) => resultMap(index));
		
		this.result = [];
		this.covnertArr.reverse().map(f => this.result.push(...f));
		
		const div = document.createElement('DIV');
		div.classList = `${this.prefix}__group ${this.prefix}__clone-list`;
		this.creatHref = this.navbar.querySelector('DIV').appendChild(div);
		
		this.result.map((f, index) => {
			const myHref = document.createElement('a');
			if (index < linkFirst) myHref.classList = `${this.prefix}__clone-item ${this.prefix}__clone--style`;
			else myHref.classList = `${this.prefix}__clone-item`;
			myHref.textContent = f.name || '';
			myHref.href = f.url || '';
			
			return this.creatHref.appendChild(myHref);
		});
	}
}
