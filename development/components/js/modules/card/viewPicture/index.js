import $render from './render';

export default class $index {
	constructor(init) {
		({
			selector: this.selector,
			delegate: this.delegate,
			prefix: this.prefix,
			productPrefix: this.productPrefix,
			productUrl: this.productUrl
		} = init);
		
		// --- ERROR ---
		this.err = undefined;
		this.warn = next => console.error('[SYSTEM]:', next);
		if (!this.selector) return this.warn(`selector is ${this.err}`);

		// --- VARIABLES ---
		this.arrCollection = document.querySelectorAll(this.selector);
	}

	__valid(prop, nextprop, index) {
		return (prop[index] && prop[index][nextprop]) ? prop[index][nextprop] : this.err;
	}

	__render(f, index) {
		this.render = new $render({
			prefix: this.prefix,
			selector: f,
			productPrefix: this.productPrefix,
			productUrl: this.__valid(this.productUrl, 'imgUrl', index)
		});
	}

	run() {
		Array.from(this.arrCollection, (f, index) => this.__render(f, index));
	}
}
