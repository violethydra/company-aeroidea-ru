export default class $render {
	constructor(init) {
		({
			prefix: this.prefix,
			selector: this.selector,
			productPrefix: this.productPrefix,
			productUrl: this.productUrl
		} = init);


		// --- ERROR ---
		this.err = undefined;
		this.warn = next => console.error('[SYSTEM]:', next);
		if (!this.selector) return this.warn(`selector is ${this.err}`);

		// --- VARIABLES ---
		this.defaultFake = 'https://api.fnkr.net/testimg/260x145/eee/ccc/?text=img';
		this.selectPicture = this.productUrl || this.defaultFake;

		if (!this.productUrl || !this.productPrefix) this.result = this.defaultFake;
		else this.result = this.productPrefix + this.productUrl;
		// ---
		this.render();
	}

	render() {
		const el = document.createElement('picture');
		el.innerHTML = `
			<source srcset="${this.result}" media="(min-width: 320px)">
			<img src="${this.result}" alt="Picture" title="Picture">
		`;
		this.selector.appendChild(el);
	}
}
