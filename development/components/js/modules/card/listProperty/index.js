import $render from './render';

export default class $index {
	constructor(init) {
		this.selector = document.querySelector(init.selector);
		this.selectorID = init.selector;
		if (!this.selector) return;

		// --- VARIABLES ---
		this.arrCollection = document.querySelectorAll(this.selectorID);
		this.prefix = init.prefix;

		this.productUrl = init.productUrl;
		this.productText = init.productText;
		this.err = undefined;
		// ---
	}

	run() {
		if (this.selector) {
			this.firstRun();
		}
	}

	firstRun() {
		this.main();
	}

	__valid(prop, nextprop, index) {
		return (prop[index] && prop[index][nextprop]) ? prop[index][nextprop] : this.err;
	}

	__render(f, index) {
		this.render = new $render({
			prefix: this.prefix,
			selector: f,
			productList: this.__valid(this.productUrl, 'params', index)
		});
	}

	main() {
		Array.from(this.arrCollection, (f, index) => this.__render(f, index));
	}
}
