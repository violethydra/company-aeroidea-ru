export default class $render {
	constructor(init) {
		// --- VARIABLES ---
		this.prefix = init.prefix;
		this.selector = init.selector;

		this.status = init.productUrl;
		this.productList = init.productList;
		this.err = undefined;
		// ---
		this.main();
	}

	__valid(prop, nextprop, index) {
		return (prop[index] && prop[index][nextprop]) ? prop[index][nextprop] : this.err;
	}
	
	__warning() {
		if (!this.productList) this.productList = [{ 0: this.err }];
	}
	
	__render() {
		
		this.productList.map((f, index) => {
			const el = document.createElement('div');
			el.classList = `${this.prefix}__list-item`;
			el.innerHTML = `
				<div class="stock__list-el">${this.__valid(this.productList, 'name', index)}</div>
				<div class="stock__list-el">${this.__valid(this.productList, 'value', index)}</div>
			`;
			return this.selector.appendChild(el);
		});

	}

	main() {
		this.__warning();
		this.__render();
	}
}
