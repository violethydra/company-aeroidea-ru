export default class $render {
	constructor(init) {
		// --- VARIABLES ---
		this.prefix = init.prefix;
		this.selector = init.selector;
		this.countStars = init.countStars + 1;
		this.activeStar = (init.activeStar >= 0 && init.activeStar <= init.countStars) ? init.activeStar : 0;
		// --- 
		this.render();
	}
	
	__next(f) {
		const activeClass = (f === this.activeStar) ? 'active' : '';
		const el = document.createElement('span');
		el.classList = `${this.prefix}__stars-el ${this.prefix}__svg ${this.prefix}__svg--star ${activeClass}`;
		el.innerHTML = '<svg role="picture-svg" class="glyphs__star"><use xlink:href="#id-glyphs-star"></use></svg>';
		this.selector.appendChild(el);
	}

	render() {
		Array.from([...Array(this.countStars).keys()], f => this.__next(f));
	}
}