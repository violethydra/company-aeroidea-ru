import $render from './render';

export default class $index {
	constructor(init) {
		({
			selector: this.selector,
			delegate: this.delegate,
			dataAttr: this.dataAttr,
			prefix: this.prefix,
			activeStar: this.activeStar,
			countStars: this.countStars
		} = init);
		console.log('init', init);
		
		// --- ERROR ---
		this.err = undefined;
		this.warn = err => console.error('[SYSTEM]:', err);
		if (!this.selector) return this.warn(`selector is ${this.err}`);

		// --- VARIABLES ---
		this.selectorParent = document.querySelector(this.delegate);
		this.arrCollection = document.querySelectorAll(this.selector);
		this.sumStars = (this.countStars >= 1 && this.countStars <= 9) ? this.countStars : this.warn(`countStars is ${this.err}`);
		this.sumActive = (this.activeStar >= 0 && this.activeStar <= this.countStars) ? this.activeStar : this.warn(`activeStar is ${this.err}`);
	}

	run() {
		this.firstRun();
		this.eventHandlers();
	}

	eventHandlers() {
		this.selectorParent.addEventListener('click', event => this.__delegateHandler(event), false);
	}

	firstRun() {
		this.render();
	}

	render() {
		Array.from(this.arrCollection, f => new $render({
			prefix: this.prefix,
			selector: f,
			countStars: this.sumStars,
			activeStar: this.sumActive
		}));
	}

	__delegateHandler(event) {
		this.event = event.target;
		this.closest = this.event.closest(this.selector);
		this.cleanAll = () => [...this.closest.children]
			.filter(f => f.classList.contains('active'))
			.forEach(f => f.classList.remove('active'));

		if (this.closest && this.event.matches(`.${this.prefix}__stars-el`)) {
			if (this.event.classList.contains('active')) {
				this.cleanAll();
				this.closest.setAttribute(this.dataAttr, 0);
				this.closest.firstElementChild.classList.add('active');
				return;
			}

			this.starIndex = [...this.event.parentElement.children].indexOf(this.event);
			this.closest.getAttribute(this.dataAttr);
			this.closest.setAttribute(this.dataAttr, this.starIndex);

			this.cleanAll();
			this.event.classList.add('active');
		}
	}
}
