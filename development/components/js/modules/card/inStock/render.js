export default class $render {
	constructor(init) {
		// --- VARIABLES ---
		this.prefix = init.prefix;
		this.selector = init.selector;

		this.status = init.productUrl;
		this.productText = init.productText;
		// ---
		this.main();
	}

	__render() {

		this.result = {
			success: `
			<div class="${this.prefix}__item ${this.prefix}__in success">
			<span class="${this.prefix}__svg ${this.prefix}__svg--checked"><svg role="picture-svg" class="glyphs__checked">
			<use xlink:href="#id-glyphs-checked"></use>
			</svg></span>
			<span>В наличии</span>
			</div>
			`,
			error: `
			<div class="${this.prefix}__item ${this.prefix}__in error">
			<span class="${this.prefix}__svg ${this.prefix}__svg--checked"><svg role="picture-svg" class="glyphs__unchecked">
			<use xlink:href="#id-glyphs-unchecked"></use>
			</svg></span>
			<span>Нет в наличии</span>
			</div>
			`
		};

		const el = document.createElement('div');
		el.classList = `${this.prefix}__group ${this.prefix}__name`;
		el.innerHTML = `
			${(!this.status) ? this.result.error : this.result.success}
			<h4 class="${this.prefix}__item ${this.prefix}__h4">${this.productText}</h4>
		`;
		this.selector.prepend(el);
	}

	main() {
		this.__render();
	}
}
