export default class $render {
	constructor(init) {
		// --- VARIABLES ---
		this.prefix = init.prefix;
		this.selector = init.selector;
		this.productFav = init.productFav || false;
		this.err = undefined;
		// ---
		this.render();
	}
	
	__render() {
		
		const el = document.createElement('div');
		el.classList = `${this.prefix}__list-item ${this.prefix}__cursor`;
		el.dataset.inFavorite = `${this.productFav}`;
		el.innerHTML = `
		<span class="${this.prefix}__svg ${this.prefix}__repain ${this.prefix}__svg--heart"><svg role="picture-svg" class="glyphs__heart">
		<use xlink:href="#id-glyphs-heart"></use>
		</svg></span>
		<span class="${this.prefix}__svg ${this.prefix}__repain ${this.prefix}__svg--heart hidden"><svg role="picture-svg" class="glyphs__heart-full">
		<use xlink:href="#id-glyphs-heart-full"></use>
		</svg></span>
		`;
		return this.selector.appendChild(el);
		
	}
	
	__status() {
		if (this.productFav) {
			[...this.selector.querySelectorAll('span')].forEach(f => f.classList.toggle('hidden'));
		}
	}

	render() {
		this.__render();
		this.__status();
	}
}
