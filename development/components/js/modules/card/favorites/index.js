import $render from './render';

export default class $index {
	constructor(init) {
		this.selector = document.querySelector(init.selector);
		this.selectorID = init.selector;
		if (!this.selector) return;

		// --- VARIABLES ---
		this.arrCollection = document.querySelectorAll(this.selectorID);
		this.selectorParent = document.querySelector(init.delegate);
		this.prefix = init.prefix;

		this.productUrl = init.productUrl;
		this.productText = init.productText;
		this.fetchURL = init.fetchURL;
		this.errorMessage = init.errorMessage;
		this.err = undefined;
		// ---
	}

	run() {
		if (this.selector) {
			this.firstRun();
			this.eventHandlers();
		}
	}

	eventHandlers() {
		this.selectorParent.addEventListener('click', event => this.favorHeart(event), false);
		this.selectorParent.addEventListener('animationend', event => this.animateEnd(event), false);
	}

	firstRun() {
		this.main();
	}

	__buildPOST() {
		this.obj = {
			products: {
				id: Number(this.id.getAttribute('data-stock-id')),
				inFav: Boolean(this.cursor.getAttribute('data-in-favorite').toLowerCase() === 'false')
			}
		};
		return console.log('this.obj', this.obj);
	}

	__switchResult(event) {
		console.log('this.closest', this.closest);
		console.log('this.id', this.id.getAttribute('data-stock-id'));
		[...this.closest.querySelectorAll('span')].forEach(f => f.classList.toggle('hidden'));
	}

	__animateStart() {
		this.cursor.classList.add('anima');
		this.__switchResult();
	}

	animateEnd(event) {
		this.cursor.classList.remove('anima');
		if (this.cursor.getAttribute('data-in-favorite') === 'false') this.cursor.setAttribute('data-in-favorite', true);
		else this.cursor.setAttribute('data-in-favorite', false);
	}

	fetchData(sendData) {
		this.url = this.fetchURL;
		this.options = {
			method: 'POST',
			mode: 'cors',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(sendData)
		};

		fetch(this.url, this.options)
			.then((onResolve) => {
				this.status = onResolve.status;
				return onResolve.json();
			})
			.then(data => (this.obj.products.id !== 2 ? JSON.stringify(data) : Promise.reject(data)))
			// .then(data => (this.status === 201 ? JSON.stringify(data) : Promise.reject(data)))
			.then(() => this.__animateStart())
			.catch(onRejected => console.error(this.errorMessage));
	}

	favorHeart(event) {
		this.event = event.target;
		this.closest = this.event.closest(this.selectorID);
		this.id = this.event.closest('[data-stock-id]');

		if (this.closest && this.event.matches(`.${this.prefix}__svg--heart`)) {
			if (this.id.getAttribute('data-stock-id') === 'undefined') return;
			this.cursor = this.closest.querySelector(`.${this.prefix}__cursor`);

			this.__buildPOST();
			this.fetchData(this.obj);
		}
	}

	__valid(prop, nextprop, index) {
		return (prop[index] && prop[index][nextprop]) ? prop[index][nextprop] : this.err;
	}

	__render(f, index) {
		this.render = new $render({
			prefix: this.prefix,
			selector: f,
			productFav: this.__valid(this.productUrl, 'inFav', index)
		});
	}

	main() {
		Array.from(this.arrCollection, (f, index) => this.__render(f, index));
	}
}
