import $wrapper from './wrapper';
import $rating from './rating';
import $viewPicture from './viewPicture';
import $inStock from './inStock';
import $listProperty from './listProperty';
import $favorites from './favorites';

export default class $cardController {
	constructor(init) {
		this.selector = document.querySelector(init.selector);
		this.selectorID = init.selector;
		
		// --- ERROR ---
		this.warn = next => console.error('[SYSTEM]:', next);
		this.err = undefined;
		if (!this.selector) return this.warn(`selector is ${this.err}`);
		
		// --- VARIABLES ---
		this.productPrefix = init.viewPicture.productPrefix;
		this.favoritesURL = init.favorites.fetchURL;

		({
			prefix: this.prefix,
			fetchURL: this.fetchURL,
			selectorJS: this.selectorJS
		} = init);

		({
			countStars: this.countStars,
			activeStar: this.activeStar,
			dataAttr: this.dataAttr
		} = init.ratingStars);

		this.err = '[SYSTEM]:';
		// ---
	}

	run() {
		this.fetchData();
	}

	resolveBuffer() {
		this.cardsLength = 5 || this.mainData.PRODUCTS_SUCCESS.data.products.length;
		this.dataProducts = this.mainData.PRODUCTS_SUCCESS.data.products;
		this.favoriteError = this.mainData.FAVORITE_FAIL.data.message;
	}

	wrapper() {
		new $wrapper({
			selector: '',
			delegate: this.selectorID,
			prefix: this.prefix,
			cardsLength: this.cardsLength,
			productUrl: this.dataProducts,
			selectorJS: this.selectorJS
		}).run();
	}

	rating() {
		new $rating({
			selector: `[${this.selectorJS.rating}]`,
			dataAttr: this.dataAttr,
			delegate: this.selectorID,
			prefix: this.prefix,
			countStars: this.countStars,
			activeStar: this.activeStar
		}).run();
	}

	viewPicture() {
		new $viewPicture({
			selector: `[${this.selectorJS.viewPicture}]`,
			delegate: this.selectorID,
			prefix: this.prefix,
			productPrefix: this.productPrefix,
			productUrl: this.dataProducts
		}).run();
	}

	inStock() {
		new $inStock({
			selector: `[${this.selectorJS.inStock}]`,
			delegate: this.selectorID,
			prefix: this.prefix,
			productUrl: this.dataProducts
		}).run();
	}

	listProperty() {
		new $listProperty({
			selector: `[${this.selectorJS.listProperty}]`,
			delegate: this.selectorID,
			prefix: this.prefix,
			productUrl: this.dataProducts
		}).run();
	}

	favorites() {
		new $favorites({
			selector: `[${this.selectorJS.favorites}]`,
			delegate: this.selectorID,
			prefix: this.prefix,
			productUrl: this.dataProducts,
			fetchURL: this.favoritesURL,
			errorMessage: this.favoriteError
		}).run();
	}

	log() {
		console.log(this.mainData);
		document.querySelector('#loader').classList.add('DELETE');
	}

	fetchData() {
		const options = {
			method: 'GET',
			headers: { Accept: 'application/json, text/plain, */*', 'Content-Type': 'application/json' },
			mode: 'cors',
			cache: 'default'
		};

		const url = this.fetchURL;

		fetch(url, options)
			.then(onResolve => onResolve.json())
			.then(data => (this.mainData = data))
			.then(this.resolveBuffer.bind(this))
			.then(this.wrapper.bind(this))
			.then(this.rating.bind(this))
			.then(this.viewPicture.bind(this))
			.then(this.inStock.bind(this))
			.then(this.listProperty.bind(this))
			.then(this.favorites.bind(this))
			.then(this.log.bind(this))
			// .catch(onRejected => console.error(this.err, `${onRejected.message}`));
	}
}
