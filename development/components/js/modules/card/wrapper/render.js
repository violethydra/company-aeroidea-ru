export default class $render {
	constructor(init) {
		// --- VARIABLES ---
		this.selector = document.querySelector(init.selector);
		this.prefix = init.prefix;
		this.cardsCode = init.cardsCode;
		this.arrName = init.arrName;
		this.selectorJS = init.selectorJS;
		this.stockID = init.stockID;
		// ---
	}

	grid() {
		return `
		<div class="${this.prefix}__group ${this.prefix}__between ${this.prefix}__column">
			<div class="${this.prefix}__item ${this.prefix}__stars" ${this.selectorJS.rating} data-rating="3">
			</div>
			<div class="${this.prefix}__item ${this.prefix}__code">Арт. ${this.cardsCode}</div>
			</div>
			<div class="${this.prefix}__item ${this.prefix}__picture" ${this.selectorJS.viewPicture}></div>
			<div class="${this.prefix}__group ${this.prefix}__info" ${this.selectorJS.inStock}>
		
			<div class="${this.prefix}__group ${this.prefix}__list" ${this.selectorJS.listProperty}></div>
			</div>
			
			<div class="${this.prefix}__group ${this.prefix}__price">
			<div class="${this.prefix}__group ${this.prefix}__price-el">
			<div class="${this.prefix}__item ${this.prefix}__price-main">49 999 руб. </div>
			<div class="${this.prefix}__item ${this.prefix}__price-bonus">+490 бонусов</div>
			</div>

			<div class="${this.prefix}__group ${this.prefix}__column ${this.prefix}__between">
			<a title="URL" role="link" href="#" class="${this.prefix}__button ${this.prefix}__button--main">
			<span class="${this.prefix}__svg ${this.prefix}__svg--cart"><svg role="picture-svg" class="glyphs__cart"><use xlink:href="#id-glyphs-cart"></use></svg></span>
			<span>Купить</span>
			</a>
			<div class="${this.prefix}__group ${this.prefix}__extra" >
			<div class="${this.prefix}__group ${this.prefix}__favorites" ${this.selectorJS.favorites}></div>

			<span class="${this.prefix}__svg ${this.prefix}__repain"><svg role="picture-svg" class="glyphs__comparison">
			<use xlink:href="#id-glyphs-comparison"></use>
			</svg></span>

			</div>
			</div>
			</div>
			`;
	}

	next(myArray) {
		const div = document.createElement('DIV');
		div.classList = `${this.prefix} ${this.prefix}__box`;
		div.dataset.stockId = `${this.stockID}`;
		div.innerHTML = myArray;

		this.selector.appendChild(div);
	}
}
