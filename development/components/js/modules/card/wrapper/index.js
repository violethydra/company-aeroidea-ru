import $render from './render';

export default class $index {
	constructor(init) {
		({
			selector: this.selector,
			delegate: this.delegate,
			prefix: this.prefix,
			cardsLength: this.cardsLength,
			productUrl: this.productUrl,
			selectorJS: this.selectorJS
		} = init);

		// --- ERROR ---
		this.err = undefined;
		this.warn = next => console.error('[SYSTEM]:', next);
		if (!this.delegate) return this.warn(`delegate is ${this.err}`);

		// --- VARIABLES ---
		this.qSelector = document.querySelector(this.delegate);
	}

	__valid(prop, el, index) {
		return (prop[index] && prop[index][el]) ? prop[index][el] : this.err;
	}

	__render(f) {
		this.render = new $render({
			selector: this.delegate,
			prefix: this.prefix,
			selectorJS: this.selectorJS,
			stockID: this.__valid(this.productUrl, 'id', f),
			cardsCode: this.__valid(this.productUrl, 'code', f)
		});
		this.end = this.render.grid();
		this.render.next(this.end);
	}

	run() {
		Array.from([...Array(this.cardsLength).keys()], f => this.__render(f));
	}
}
