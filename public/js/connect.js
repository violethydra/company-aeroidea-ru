/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./development/components/js/connect.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./development/components/js/connect.js":
/*!**********************************************!*\
  !*** ./development/components/js/connect.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modules_card_cardController__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modules/card/cardController */ "./development/components/js/modules/card/cardController.js");
// ============================
//    Name: index.js
// ============================


var startPackage = function startPackage(option) {
  console.log('[DOM]:', 'DOMContentLoaded', option);
  new _modules_card_cardController__WEBPACK_IMPORTED_MODULE_0__["default"]({
    prefix: 'stock',
    selector: '[data-js=controller]',
    fetchURL: './my-server.json',
    // fetchURL: 'https://my-json-server.typicode.com/aero-frontend/test-task/db',
    selectorJS: {
      rating: 'data-js=rating',
      viewPicture: 'data-js=viewPicture',
      inStock: 'data-js=inStock',
      listProperty: 'data-js=listProperty',
      favorites: 'data-js=favorites'
    },
    ratingStars: {
      dataAttr: 'data-rating',
      countStars: 5,
      activeStar: 3
    },
    viewPicture: {
      productPrefix: './media/img/',
      productUrl: ''
    },
    favorites: {
      fetchURL: 'https://jsonplaceholder.typicode.com/posts' // fetchURL: 'https://jsonplaceholder.typicode.com/posts/1'

    }
  }).run();
};

var start = function start(option) {
  if (option === true) startPackage(option);else console.error('System: ', 'I think shit happens 😥 ');
};

var addCss = function addCss(fileName) {
  var link = document.createElement('link');
  link.type = 'text/css';
  link.rel = 'stylesheet';
  link.href = fileName;
  document.head.appendChild(link);
};

if (typeof window !== 'undefined' && window && window.addEventListener) {
  if (navigator.userAgent.includes('Firefox')) addCss('css/firefox.css');
  if (navigator.userAgent.includes('Edge')) addCss('css/edge.css');
  document.addEventListener('DOMContentLoaded', start(true), false);
}

/***/ }),

/***/ "./development/components/js/modules/card/cardController.js":
/*!******************************************************************!*\
  !*** ./development/components/js/modules/card/cardController.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $cardController; });
/* harmony import */ var _wrapper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./wrapper */ "./development/components/js/modules/card/wrapper/index.js");
/* harmony import */ var _rating__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./rating */ "./development/components/js/modules/card/rating/index.js");
/* harmony import */ var _viewPicture__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./viewPicture */ "./development/components/js/modules/card/viewPicture/index.js");
/* harmony import */ var _inStock__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./inStock */ "./development/components/js/modules/card/inStock/index.js");
/* harmony import */ var _listProperty__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./listProperty */ "./development/components/js/modules/card/listProperty/index.js");
/* harmony import */ var _favorites__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./favorites */ "./development/components/js/modules/card/favorites/index.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }








var $cardController =
/*#__PURE__*/
function () {
  function $cardController(init) {
    _classCallCheck(this, $cardController);

    this.selector = document.querySelector(init.selector);
    this.selectorID = init.selector; // --- ERROR ---

    this.warn = function (next) {
      return console.error('[SYSTEM]:', next);
    };

    this.err = undefined;
    if (!this.selector) return this.warn("selector is ".concat(this.err)); // --- VARIABLES ---

    this.productPrefix = init.viewPicture.productPrefix;
    this.favoritesURL = init.favorites.fetchURL;
    this.prefix = init.prefix;
    this.fetchURL = init.fetchURL;
    this.selectorJS = init.selectorJS;
    var _init$ratingStars = init.ratingStars;
    this.countStars = _init$ratingStars.countStars;
    this.activeStar = _init$ratingStars.activeStar;
    this.dataAttr = _init$ratingStars.dataAttr;
    this.err = '[SYSTEM]:'; // ---
  }

  _createClass($cardController, [{
    key: "run",
    value: function run() {
      this.fetchData();
    }
  }, {
    key: "resolveBuffer",
    value: function resolveBuffer() {
      this.cardsLength = 5 || false;
      this.dataProducts = this.mainData.PRODUCTS_SUCCESS.data.products;
      this.favoriteError = this.mainData.FAVORITE_FAIL.data.message;
    }
  }, {
    key: "wrapper",
    value: function wrapper() {
      new _wrapper__WEBPACK_IMPORTED_MODULE_0__["default"]({
        selector: '',
        delegate: this.selectorID,
        prefix: this.prefix,
        cardsLength: this.cardsLength,
        productUrl: this.dataProducts,
        selectorJS: this.selectorJS
      }).run();
    }
  }, {
    key: "rating",
    value: function rating() {
      new _rating__WEBPACK_IMPORTED_MODULE_1__["default"]({
        selector: "[".concat(this.selectorJS.rating, "]"),
        dataAttr: this.dataAttr,
        delegate: this.selectorID,
        prefix: this.prefix,
        countStars: this.countStars,
        activeStar: this.activeStar
      }).run();
    }
  }, {
    key: "viewPicture",
    value: function viewPicture() {
      new _viewPicture__WEBPACK_IMPORTED_MODULE_2__["default"]({
        selector: "[".concat(this.selectorJS.viewPicture, "]"),
        delegate: this.selectorID,
        prefix: this.prefix,
        productPrefix: this.productPrefix,
        productUrl: this.dataProducts
      }).run();
    }
  }, {
    key: "inStock",
    value: function inStock() {
      new _inStock__WEBPACK_IMPORTED_MODULE_3__["default"]({
        selector: "[".concat(this.selectorJS.inStock, "]"),
        delegate: this.selectorID,
        prefix: this.prefix,
        productUrl: this.dataProducts
      }).run();
    }
  }, {
    key: "listProperty",
    value: function listProperty() {
      new _listProperty__WEBPACK_IMPORTED_MODULE_4__["default"]({
        selector: "[".concat(this.selectorJS.listProperty, "]"),
        delegate: this.selectorID,
        prefix: this.prefix,
        productUrl: this.dataProducts
      }).run();
    }
  }, {
    key: "favorites",
    value: function favorites() {
      new _favorites__WEBPACK_IMPORTED_MODULE_5__["default"]({
        selector: "[".concat(this.selectorJS.favorites, "]"),
        delegate: this.selectorID,
        prefix: this.prefix,
        productUrl: this.dataProducts,
        fetchURL: this.favoritesURL,
        errorMessage: this.favoriteError
      }).run();
    }
  }, {
    key: "log",
    value: function log() {
      console.log(this.mainData);
      document.querySelector('#loader').classList.add('DELETE');
    }
  }, {
    key: "fetchData",
    value: function fetchData() {
      var _this = this;

      var options = {
        method: 'GET',
        headers: {
          Accept: 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        mode: 'cors',
        cache: 'default'
      };
      var url = this.fetchURL;
      fetch(url, options).then(function (onResolve) {
        return onResolve.json();
      }).then(function (data) {
        return _this.mainData = data;
      }).then(this.resolveBuffer.bind(this)).then(this.wrapper.bind(this)).then(this.rating.bind(this)).then(this.viewPicture.bind(this)).then(this.inStock.bind(this)).then(this.listProperty.bind(this)).then(this.favorites.bind(this)).then(this.log.bind(this)); // .catch(onRejected => console.error(this.err, `${onRejected.message}`));
    }
  }]);

  return $cardController;
}();



/***/ }),

/***/ "./development/components/js/modules/card/favorites/index.js":
/*!*******************************************************************!*\
  !*** ./development/components/js/modules/card/favorites/index.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $index; });
/* harmony import */ var _render__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./render */ "./development/components/js/modules/card/favorites/render.js");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var $index =
/*#__PURE__*/
function () {
  function $index(init) {
    _classCallCheck(this, $index);

    this.selector = document.querySelector(init.selector);
    this.selectorID = init.selector;
    if (!this.selector) return; // --- VARIABLES ---

    this.arrCollection = document.querySelectorAll(this.selectorID);
    this.selectorParent = document.querySelector(init.delegate);
    this.prefix = init.prefix;
    this.productUrl = init.productUrl;
    this.productText = init.productText;
    this.fetchURL = init.fetchURL;
    this.errorMessage = init.errorMessage;
    this.err = undefined; // ---
  }

  _createClass($index, [{
    key: "run",
    value: function run() {
      if (this.selector) {
        this.firstRun();
        this.eventHandlers();
      }
    }
  }, {
    key: "eventHandlers",
    value: function eventHandlers() {
      var _this = this;

      this.selectorParent.addEventListener('click', function (event) {
        return _this.favorHeart(event);
      }, false);
      this.selectorParent.addEventListener('animationend', function (event) {
        return _this.animateEnd(event);
      }, false);
    }
  }, {
    key: "firstRun",
    value: function firstRun() {
      this.main();
    }
  }, {
    key: "__buildPOST",
    value: function __buildPOST() {
      this.obj = {
        products: {
          id: Number(this.id.getAttribute('data-stock-id')),
          inFav: Boolean(this.cursor.getAttribute('data-in-favorite').toLowerCase() === 'false')
        }
      };
      return console.log('this.obj', this.obj);
    }
  }, {
    key: "__switchResult",
    value: function __switchResult(event) {
      console.log('this.closest', this.closest);
      console.log('this.id', this.id.getAttribute('data-stock-id'));

      _toConsumableArray(this.closest.querySelectorAll('span')).forEach(function (f) {
        return f.classList.toggle('hidden');
      });
    }
  }, {
    key: "__animateStart",
    value: function __animateStart() {
      this.cursor.classList.add('anima');

      this.__switchResult();
    }
  }, {
    key: "animateEnd",
    value: function animateEnd(event) {
      this.cursor.classList.remove('anima');
      if (this.cursor.getAttribute('data-in-favorite') === 'false') this.cursor.setAttribute('data-in-favorite', true);else this.cursor.setAttribute('data-in-favorite', false);
    }
  }, {
    key: "fetchData",
    value: function fetchData(sendData) {
      var _this2 = this;

      this.url = this.fetchURL;
      this.options = {
        method: 'POST',
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(sendData)
      };
      fetch(this.url, this.options).then(function (onResolve) {
        _this2.status = onResolve.status;
        return onResolve.json();
      }).then(function (data) {
        return _this2.obj.products.id !== 2 ? JSON.stringify(data) : Promise.reject(data);
      }) // .then(data => (this.status === 201 ? JSON.stringify(data) : Promise.reject(data)))
      .then(function () {
        return _this2.__animateStart();
      })["catch"](function (onRejected) {
        return console.error(_this2.errorMessage);
      });
    }
  }, {
    key: "favorHeart",
    value: function favorHeart(event) {
      this.event = event.target;
      this.closest = this.event.closest(this.selectorID);
      this.id = this.event.closest('[data-stock-id]');

      if (this.closest && this.event.matches(".".concat(this.prefix, "__svg--heart"))) {
        if (this.id.getAttribute('data-stock-id') === 'undefined') return;
        this.cursor = this.closest.querySelector(".".concat(this.prefix, "__cursor"));

        this.__buildPOST();

        this.fetchData(this.obj);
      }
    }
  }, {
    key: "__valid",
    value: function __valid(prop, nextprop, index) {
      return prop[index] && prop[index][nextprop] ? prop[index][nextprop] : this.err;
    }
  }, {
    key: "__render",
    value: function __render(f, index) {
      this.render = new _render__WEBPACK_IMPORTED_MODULE_0__["default"]({
        prefix: this.prefix,
        selector: f,
        productFav: this.__valid(this.productUrl, 'inFav', index)
      });
    }
  }, {
    key: "main",
    value: function main() {
      var _this3 = this;

      Array.from(this.arrCollection, function (f, index) {
        return _this3.__render(f, index);
      });
    }
  }]);

  return $index;
}();



/***/ }),

/***/ "./development/components/js/modules/card/favorites/render.js":
/*!********************************************************************!*\
  !*** ./development/components/js/modules/card/favorites/render.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $render; });
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var $render =
/*#__PURE__*/
function () {
  function $render(init) {
    _classCallCheck(this, $render);

    // --- VARIABLES ---
    this.prefix = init.prefix;
    this.selector = init.selector;
    this.productFav = init.productFav || false;
    this.err = undefined; // ---

    this.render();
  }

  _createClass($render, [{
    key: "__render",
    value: function __render() {
      var el = document.createElement('div');
      el.classList = "".concat(this.prefix, "__list-item ").concat(this.prefix, "__cursor");
      el.dataset.inFavorite = "".concat(this.productFav);
      el.innerHTML = "\n\t\t<span class=\"".concat(this.prefix, "__svg ").concat(this.prefix, "__repain ").concat(this.prefix, "__svg--heart\"><svg role=\"picture-svg\" class=\"glyphs__heart\">\n\t\t<use xlink:href=\"#id-glyphs-heart\"></use>\n\t\t</svg></span>\n\t\t<span class=\"").concat(this.prefix, "__svg ").concat(this.prefix, "__repain ").concat(this.prefix, "__svg--heart hidden\"><svg role=\"picture-svg\" class=\"glyphs__heart-full\">\n\t\t<use xlink:href=\"#id-glyphs-heart-full\"></use>\n\t\t</svg></span>\n\t\t");
      return this.selector.appendChild(el);
    }
  }, {
    key: "__status",
    value: function __status() {
      if (this.productFav) {
        _toConsumableArray(this.selector.querySelectorAll('span')).forEach(function (f) {
          return f.classList.toggle('hidden');
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      this.__render();

      this.__status();
    }
  }]);

  return $render;
}();



/***/ }),

/***/ "./development/components/js/modules/card/inStock/index.js":
/*!*****************************************************************!*\
  !*** ./development/components/js/modules/card/inStock/index.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $index; });
/* harmony import */ var _render__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./render */ "./development/components/js/modules/card/inStock/render.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var $index =
/*#__PURE__*/
function () {
  function $index(init) {
    _classCallCheck(this, $index);

    this.selector = document.querySelector(init.selector);
    this.selectorID = init.selector;
    if (!this.selector) return; // --- VARIABLES ---

    this.arrCollection = document.querySelectorAll(this.selectorID);
    this.prefix = init.prefix;
    this.productUrl = init.productUrl;
    this.productText = init.productText;
    this.err = undefined; // ---
  }

  _createClass($index, [{
    key: "run",
    value: function run() {
      if (this.selector) {
        this.firstRun();
      }
    }
  }, {
    key: "firstRun",
    value: function firstRun() {
      this.main();
    }
  }, {
    key: "__valid",
    value: function __valid(prop, nextprop, index) {
      return prop[index] && prop[index][nextprop] ? prop[index][nextprop] : this.err;
    }
  }, {
    key: "__render",
    value: function __render(f, index) {
      this.render = new _render__WEBPACK_IMPORTED_MODULE_0__["default"]({
        prefix: this.prefix,
        selector: f,
        productUrl: this.__valid(this.productUrl, 'availability', index),
        productText: this.__valid(this.productUrl, 'title', index)
      });
    }
  }, {
    key: "main",
    value: function main() {
      var _this = this;

      Array.from(this.arrCollection, function (f, index) {
        return _this.__render(f, index);
      });
    }
  }]);

  return $index;
}();



/***/ }),

/***/ "./development/components/js/modules/card/inStock/render.js":
/*!******************************************************************!*\
  !*** ./development/components/js/modules/card/inStock/render.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $render; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var $render =
/*#__PURE__*/
function () {
  function $render(init) {
    _classCallCheck(this, $render);

    // --- VARIABLES ---
    this.prefix = init.prefix;
    this.selector = init.selector;
    this.status = init.productUrl;
    this.productText = init.productText; // ---

    this.main();
  }

  _createClass($render, [{
    key: "__render",
    value: function __render() {
      this.result = {
        success: "\n\t\t\t<div class=\"".concat(this.prefix, "__item ").concat(this.prefix, "__in success\">\n\t\t\t<span class=\"").concat(this.prefix, "__svg ").concat(this.prefix, "__svg--checked\"><svg role=\"picture-svg\" class=\"glyphs__checked\">\n\t\t\t<use xlink:href=\"#id-glyphs-checked\"></use>\n\t\t\t</svg></span>\n\t\t\t<span>\u0412 \u043D\u0430\u043B\u0438\u0447\u0438\u0438</span>\n\t\t\t</div>\n\t\t\t"),
        error: "\n\t\t\t<div class=\"".concat(this.prefix, "__item ").concat(this.prefix, "__in error\">\n\t\t\t<span class=\"").concat(this.prefix, "__svg ").concat(this.prefix, "__svg--checked\"><svg role=\"picture-svg\" class=\"glyphs__unchecked\">\n\t\t\t<use xlink:href=\"#id-glyphs-unchecked\"></use>\n\t\t\t</svg></span>\n\t\t\t<span>\u041D\u0435\u0442 \u0432 \u043D\u0430\u043B\u0438\u0447\u0438\u0438</span>\n\t\t\t</div>\n\t\t\t")
      };
      var el = document.createElement('div');
      el.classList = "".concat(this.prefix, "__group ").concat(this.prefix, "__name");
      el.innerHTML = "\n\t\t\t".concat(!this.status ? this.result.error : this.result.success, "\n\t\t\t<h4 class=\"").concat(this.prefix, "__item ").concat(this.prefix, "__h4\">").concat(this.productText, "</h4>\n\t\t");
      this.selector.prepend(el);
    }
  }, {
    key: "main",
    value: function main() {
      this.__render();
    }
  }]);

  return $render;
}();



/***/ }),

/***/ "./development/components/js/modules/card/listProperty/index.js":
/*!**********************************************************************!*\
  !*** ./development/components/js/modules/card/listProperty/index.js ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $index; });
/* harmony import */ var _render__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./render */ "./development/components/js/modules/card/listProperty/render.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var $index =
/*#__PURE__*/
function () {
  function $index(init) {
    _classCallCheck(this, $index);

    this.selector = document.querySelector(init.selector);
    this.selectorID = init.selector;
    if (!this.selector) return; // --- VARIABLES ---

    this.arrCollection = document.querySelectorAll(this.selectorID);
    this.prefix = init.prefix;
    this.productUrl = init.productUrl;
    this.productText = init.productText;
    this.err = undefined; // ---
  }

  _createClass($index, [{
    key: "run",
    value: function run() {
      if (this.selector) {
        this.firstRun();
      }
    }
  }, {
    key: "firstRun",
    value: function firstRun() {
      this.main();
    }
  }, {
    key: "__valid",
    value: function __valid(prop, nextprop, index) {
      return prop[index] && prop[index][nextprop] ? prop[index][nextprop] : this.err;
    }
  }, {
    key: "__render",
    value: function __render(f, index) {
      this.render = new _render__WEBPACK_IMPORTED_MODULE_0__["default"]({
        prefix: this.prefix,
        selector: f,
        productList: this.__valid(this.productUrl, 'params', index)
      });
    }
  }, {
    key: "main",
    value: function main() {
      var _this = this;

      Array.from(this.arrCollection, function (f, index) {
        return _this.__render(f, index);
      });
    }
  }]);

  return $index;
}();



/***/ }),

/***/ "./development/components/js/modules/card/listProperty/render.js":
/*!***********************************************************************!*\
  !*** ./development/components/js/modules/card/listProperty/render.js ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $render; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var $render =
/*#__PURE__*/
function () {
  function $render(init) {
    _classCallCheck(this, $render);

    // --- VARIABLES ---
    this.prefix = init.prefix;
    this.selector = init.selector;
    this.status = init.productUrl;
    this.productList = init.productList;
    this.err = undefined; // ---

    this.main();
  }

  _createClass($render, [{
    key: "__valid",
    value: function __valid(prop, nextprop, index) {
      return prop[index] && prop[index][nextprop] ? prop[index][nextprop] : this.err;
    }
  }, {
    key: "__warning",
    value: function __warning() {
      if (!this.productList) this.productList = [{
        0: this.err
      }];
    }
  }, {
    key: "__render",
    value: function __render() {
      var _this = this;

      this.productList.map(function (f, index) {
        var el = document.createElement('div');
        el.classList = "".concat(_this.prefix, "__list-item");
        el.innerHTML = "\n\t\t\t\t<div class=\"stock__list-el\">".concat(_this.__valid(_this.productList, 'name', index), "</div>\n\t\t\t\t<div class=\"stock__list-el\">").concat(_this.__valid(_this.productList, 'value', index), "</div>\n\t\t\t");
        return _this.selector.appendChild(el);
      });
    }
  }, {
    key: "main",
    value: function main() {
      this.__warning();

      this.__render();
    }
  }]);

  return $render;
}();



/***/ }),

/***/ "./development/components/js/modules/card/rating/index.js":
/*!****************************************************************!*\
  !*** ./development/components/js/modules/card/rating/index.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $index; });
/* harmony import */ var _render__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./render */ "./development/components/js/modules/card/rating/render.js");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var $index =
/*#__PURE__*/
function () {
  function $index(init) {
    _classCallCheck(this, $index);

    this.selector = init.selector;
    this.delegate = init.delegate;
    this.dataAttr = init.dataAttr;
    this.prefix = init.prefix;
    this.activeStar = init.activeStar;
    this.countStars = init.countStars;
    console.log('init', init); // --- ERROR ---

    this.err = undefined;

    this.warn = function (err) {
      return console.error('[SYSTEM]:', err);
    };

    if (!this.selector) return this.warn("selector is ".concat(this.err)); // --- VARIABLES ---

    this.selectorParent = document.querySelector(this.delegate);
    this.arrCollection = document.querySelectorAll(this.selector);
    this.sumStars = this.countStars >= 1 && this.countStars <= 9 ? this.countStars : this.warn("countStars is ".concat(this.err));
    this.sumActive = this.activeStar >= 0 && this.activeStar <= this.countStars ? this.activeStar : this.warn("activeStar is ".concat(this.err));
  }

  _createClass($index, [{
    key: "run",
    value: function run() {
      this.firstRun();
      this.eventHandlers();
    }
  }, {
    key: "eventHandlers",
    value: function eventHandlers() {
      var _this = this;

      this.selectorParent.addEventListener('click', function (event) {
        return _this.__delegateHandler(event);
      }, false);
    }
  }, {
    key: "firstRun",
    value: function firstRun() {
      this.render();
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      Array.from(this.arrCollection, function (f) {
        return new _render__WEBPACK_IMPORTED_MODULE_0__["default"]({
          prefix: _this2.prefix,
          selector: f,
          countStars: _this2.sumStars,
          activeStar: _this2.sumActive
        });
      });
    }
  }, {
    key: "__delegateHandler",
    value: function __delegateHandler(event) {
      var _this3 = this;

      this.event = event.target;
      this.closest = this.event.closest(this.selector);

      this.cleanAll = function () {
        return _toConsumableArray(_this3.closest.children).filter(function (f) {
          return f.classList.contains('active');
        }).forEach(function (f) {
          return f.classList.remove('active');
        });
      };

      if (this.closest && this.event.matches(".".concat(this.prefix, "__stars-el"))) {
        if (this.event.classList.contains('active')) {
          this.cleanAll();
          this.closest.setAttribute(this.dataAttr, 0);
          this.closest.firstElementChild.classList.add('active');
          return;
        }

        this.starIndex = _toConsumableArray(this.event.parentElement.children).indexOf(this.event);
        this.closest.getAttribute(this.dataAttr);
        this.closest.setAttribute(this.dataAttr, this.starIndex);
        this.cleanAll();
        this.event.classList.add('active');
      }
    }
  }]);

  return $index;
}();



/***/ }),

/***/ "./development/components/js/modules/card/rating/render.js":
/*!*****************************************************************!*\
  !*** ./development/components/js/modules/card/rating/render.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $render; });
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var $render =
/*#__PURE__*/
function () {
  function $render(init) {
    _classCallCheck(this, $render);

    // --- VARIABLES ---
    this.prefix = init.prefix;
    this.selector = init.selector;
    this.countStars = init.countStars + 1;
    this.activeStar = init.activeStar >= 0 && init.activeStar <= init.countStars ? init.activeStar : 0; // --- 

    this.render();
  }

  _createClass($render, [{
    key: "__next",
    value: function __next(f) {
      var activeClass = f === this.activeStar ? 'active' : '';
      var el = document.createElement('span');
      el.classList = "".concat(this.prefix, "__stars-el ").concat(this.prefix, "__svg ").concat(this.prefix, "__svg--star ").concat(activeClass);
      el.innerHTML = '<svg role="picture-svg" class="glyphs__star"><use xlink:href="#id-glyphs-star"></use></svg>';
      this.selector.appendChild(el);
    }
  }, {
    key: "render",
    value: function render() {
      var _this = this;

      Array.from(_toConsumableArray(Array(this.countStars).keys()), function (f) {
        return _this.__next(f);
      });
    }
  }]);

  return $render;
}();



/***/ }),

/***/ "./development/components/js/modules/card/viewPicture/index.js":
/*!*********************************************************************!*\
  !*** ./development/components/js/modules/card/viewPicture/index.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $index; });
/* harmony import */ var _render__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./render */ "./development/components/js/modules/card/viewPicture/render.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var $index =
/*#__PURE__*/
function () {
  function $index(init) {
    _classCallCheck(this, $index);

    this.selector = init.selector;
    this.delegate = init.delegate;
    this.prefix = init.prefix;
    this.productPrefix = init.productPrefix;
    this.productUrl = init.productUrl;
    // --- ERROR ---
    this.err = undefined;

    this.warn = function (next) {
      return console.error('[SYSTEM]:', next);
    };

    if (!this.selector) return this.warn("selector is ".concat(this.err)); // --- VARIABLES ---

    this.arrCollection = document.querySelectorAll(this.selector);
  }

  _createClass($index, [{
    key: "__valid",
    value: function __valid(prop, nextprop, index) {
      return prop[index] && prop[index][nextprop] ? prop[index][nextprop] : this.err;
    }
  }, {
    key: "__render",
    value: function __render(f, index) {
      this.render = new _render__WEBPACK_IMPORTED_MODULE_0__["default"]({
        prefix: this.prefix,
        selector: f,
        productPrefix: this.productPrefix,
        productUrl: this.__valid(this.productUrl, 'imgUrl', index)
      });
    }
  }, {
    key: "run",
    value: function run() {
      var _this = this;

      Array.from(this.arrCollection, function (f, index) {
        return _this.__render(f, index);
      });
    }
  }]);

  return $index;
}();



/***/ }),

/***/ "./development/components/js/modules/card/viewPicture/render.js":
/*!**********************************************************************!*\
  !*** ./development/components/js/modules/card/viewPicture/render.js ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $render; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var $render =
/*#__PURE__*/
function () {
  function $render(init) {
    _classCallCheck(this, $render);

    this.prefix = init.prefix;
    this.selector = init.selector;
    this.productPrefix = init.productPrefix;
    this.productUrl = init.productUrl;
    // --- ERROR ---
    this.err = undefined;

    this.warn = function (next) {
      return console.error('[SYSTEM]:', next);
    };

    if (!this.selector) return this.warn("selector is ".concat(this.err)); // --- VARIABLES ---

    this.defaultFake = 'https://api.fnkr.net/testimg/260x145/eee/ccc/?text=img';
    this.selectPicture = this.productUrl || this.defaultFake;
    if (!this.productUrl || !this.productPrefix) this.result = this.defaultFake;else this.result = this.productPrefix + this.productUrl; // ---

    this.render();
  }

  _createClass($render, [{
    key: "render",
    value: function render() {
      var el = document.createElement('picture');
      el.innerHTML = "\n\t\t\t<source srcset=\"".concat(this.result, "\" media=\"(min-width: 320px)\">\n\t\t\t<img src=\"").concat(this.result, "\" alt=\"Picture\" title=\"Picture\">\n\t\t");
      this.selector.appendChild(el);
    }
  }]);

  return $render;
}();



/***/ }),

/***/ "./development/components/js/modules/card/wrapper/index.js":
/*!*****************************************************************!*\
  !*** ./development/components/js/modules/card/wrapper/index.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $index; });
/* harmony import */ var _render__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./render */ "./development/components/js/modules/card/wrapper/render.js");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var $index =
/*#__PURE__*/
function () {
  function $index(init) {
    _classCallCheck(this, $index);

    this.selector = init.selector;
    this.delegate = init.delegate;
    this.prefix = init.prefix;
    this.cardsLength = init.cardsLength;
    this.productUrl = init.productUrl;
    this.selectorJS = init.selectorJS;
    // --- ERROR ---
    this.err = undefined;

    this.warn = function (next) {
      return console.error('[SYSTEM]:', next);
    };

    if (!this.delegate) return this.warn("delegate is ".concat(this.err)); // --- VARIABLES ---

    this.qSelector = document.querySelector(this.delegate);
  }

  _createClass($index, [{
    key: "__valid",
    value: function __valid(prop, el, index) {
      return prop[index] && prop[index][el] ? prop[index][el] : this.err;
    }
  }, {
    key: "__render",
    value: function __render(f) {
      this.render = new _render__WEBPACK_IMPORTED_MODULE_0__["default"]({
        selector: this.delegate,
        prefix: this.prefix,
        selectorJS: this.selectorJS,
        stockID: this.__valid(this.productUrl, 'id', f),
        cardsCode: this.__valid(this.productUrl, 'code', f)
      });
      this.end = this.render.grid();
      this.render.next(this.end);
    }
  }, {
    key: "run",
    value: function run() {
      var _this = this;

      Array.from(_toConsumableArray(Array(this.cardsLength).keys()), function (f) {
        return _this.__render(f);
      });
    }
  }]);

  return $index;
}();



/***/ }),

/***/ "./development/components/js/modules/card/wrapper/render.js":
/*!******************************************************************!*\
  !*** ./development/components/js/modules/card/wrapper/render.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $render; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var $render =
/*#__PURE__*/
function () {
  function $render(init) {
    _classCallCheck(this, $render);

    // --- VARIABLES ---
    this.selector = document.querySelector(init.selector);
    this.prefix = init.prefix;
    this.cardsCode = init.cardsCode;
    this.arrName = init.arrName;
    this.selectorJS = init.selectorJS;
    this.stockID = init.stockID; // ---
  }

  _createClass($render, [{
    key: "grid",
    value: function grid() {
      return "\n\t\t<div class=\"".concat(this.prefix, "__group ").concat(this.prefix, "__between ").concat(this.prefix, "__column\">\n\t\t\t<div class=\"").concat(this.prefix, "__item ").concat(this.prefix, "__stars\" ").concat(this.selectorJS.rating, " data-rating=\"3\">\n\t\t\t</div>\n\t\t\t<div class=\"").concat(this.prefix, "__item ").concat(this.prefix, "__code\">\u0410\u0440\u0442. ").concat(this.cardsCode, "</div>\n\t\t\t</div>\n\t\t\t<div class=\"").concat(this.prefix, "__item ").concat(this.prefix, "__picture\" ").concat(this.selectorJS.viewPicture, "></div>\n\t\t\t<div class=\"").concat(this.prefix, "__group ").concat(this.prefix, "__info\" ").concat(this.selectorJS.inStock, ">\n\t\t\n\t\t\t<div class=\"").concat(this.prefix, "__group ").concat(this.prefix, "__list\" ").concat(this.selectorJS.listProperty, "></div>\n\t\t\t</div>\n\t\t\t\n\t\t\t<div class=\"").concat(this.prefix, "__group ").concat(this.prefix, "__price\">\n\t\t\t<div class=\"").concat(this.prefix, "__group ").concat(this.prefix, "__price-el\">\n\t\t\t<div class=\"").concat(this.prefix, "__item ").concat(this.prefix, "__price-main\">49 999 \u0440\u0443\u0431. </div>\n\t\t\t<div class=\"").concat(this.prefix, "__item ").concat(this.prefix, "__price-bonus\">+490 \u0431\u043E\u043D\u0443\u0441\u043E\u0432</div>\n\t\t\t</div>\n\n\t\t\t<div class=\"").concat(this.prefix, "__group ").concat(this.prefix, "__column ").concat(this.prefix, "__between\">\n\t\t\t<a title=\"URL\" role=\"link\" href=\"#\" class=\"").concat(this.prefix, "__button ").concat(this.prefix, "__button--main\">\n\t\t\t<span class=\"").concat(this.prefix, "__svg ").concat(this.prefix, "__svg--cart\"><svg role=\"picture-svg\" class=\"glyphs__cart\"><use xlink:href=\"#id-glyphs-cart\"></use></svg></span>\n\t\t\t<span>\u041A\u0443\u043F\u0438\u0442\u044C</span>\n\t\t\t</a>\n\t\t\t<div class=\"").concat(this.prefix, "__group ").concat(this.prefix, "__extra\" >\n\t\t\t<div class=\"").concat(this.prefix, "__group ").concat(this.prefix, "__favorites\" ").concat(this.selectorJS.favorites, "></div>\n\n\t\t\t<span class=\"").concat(this.prefix, "__svg ").concat(this.prefix, "__repain\"><svg role=\"picture-svg\" class=\"glyphs__comparison\">\n\t\t\t<use xlink:href=\"#id-glyphs-comparison\"></use>\n\t\t\t</svg></span>\n\n\t\t\t</div>\n\t\t\t</div>\n\t\t\t</div>\n\t\t\t");
    }
  }, {
    key: "next",
    value: function next(myArray) {
      var div = document.createElement('DIV');
      div.classList = "".concat(this.prefix, " ").concat(this.prefix, "__box");
      div.dataset.stockId = "".concat(this.stockID);
      div.innerHTML = myArray;
      this.selector.appendChild(div);
    }
  }]);

  return $render;
}();



/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29ubmVjdC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL2Nvbm5lY3QuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL2NhcmQvY2FyZENvbnRyb2xsZXIuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL2NhcmQvZmF2b3JpdGVzL2luZGV4LmpzIiwid2VicGFjazovLy8uL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvbW9kdWxlcy9jYXJkL2Zhdm9yaXRlcy9yZW5kZXIuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL2NhcmQvaW5TdG9jay9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL21vZHVsZXMvY2FyZC9pblN0b2NrL3JlbmRlci5qcyIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL21vZHVsZXMvY2FyZC9saXN0UHJvcGVydHkvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL2NhcmQvbGlzdFByb3BlcnR5L3JlbmRlci5qcyIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL21vZHVsZXMvY2FyZC9yYXRpbmcvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL2NhcmQvcmF0aW5nL3JlbmRlci5qcyIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL21vZHVsZXMvY2FyZC92aWV3UGljdHVyZS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL21vZHVsZXMvY2FyZC92aWV3UGljdHVyZS9yZW5kZXIuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL2NhcmQvd3JhcHBlci9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL21vZHVsZXMvY2FyZC93cmFwcGVyL3JlbmRlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvY29ubmVjdC5qc1wiKTtcbiIsIi8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuLy8gICAgTmFtZTogaW5kZXguanNcclxuLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG5cclxuaW1wb3J0ICRjYXJkQ29udHJvbGxlciBmcm9tICcuL21vZHVsZXMvY2FyZC9jYXJkQ29udHJvbGxlcic7XHJcblxyXG5jb25zdCBzdGFydFBhY2thZ2UgPSAob3B0aW9uKSA9PiB7XHJcblx0Y29uc29sZS5sb2coJ1tET01dOicsICdET01Db250ZW50TG9hZGVkJywgb3B0aW9uKTtcclxuXHJcblx0bmV3ICRjYXJkQ29udHJvbGxlcih7XHJcblx0XHRwcmVmaXg6ICdzdG9jaycsXHJcblx0XHRzZWxlY3RvcjogJ1tkYXRhLWpzPWNvbnRyb2xsZXJdJyxcclxuXHRcdGZldGNoVVJMOiAnLi9teS1zZXJ2ZXIuanNvbicsXHJcblx0XHQvLyBmZXRjaFVSTDogJ2h0dHBzOi8vbXktanNvbi1zZXJ2ZXIudHlwaWNvZGUuY29tL2Flcm8tZnJvbnRlbmQvdGVzdC10YXNrL2RiJyxcclxuXHRcdHNlbGVjdG9ySlM6IHtcclxuXHRcdFx0cmF0aW5nOiAnZGF0YS1qcz1yYXRpbmcnLFxyXG5cdFx0XHR2aWV3UGljdHVyZTogJ2RhdGEtanM9dmlld1BpY3R1cmUnLFxyXG5cdFx0XHRpblN0b2NrOiAnZGF0YS1qcz1pblN0b2NrJyxcclxuXHRcdFx0bGlzdFByb3BlcnR5OiAnZGF0YS1qcz1saXN0UHJvcGVydHknLFxyXG5cdFx0XHRmYXZvcml0ZXM6ICdkYXRhLWpzPWZhdm9yaXRlcydcclxuXHRcdH0sXHJcblx0XHRyYXRpbmdTdGFyczoge1xyXG5cdFx0XHRkYXRhQXR0cjogJ2RhdGEtcmF0aW5nJyxcclxuXHRcdFx0Y291bnRTdGFyczogNSxcclxuXHRcdFx0YWN0aXZlU3RhcjogM1xyXG5cdFx0fSxcclxuXHRcdHZpZXdQaWN0dXJlOiB7XHJcblx0XHRcdHByb2R1Y3RQcmVmaXg6ICcuL21lZGlhL2ltZy8nLFxyXG5cdFx0XHRwcm9kdWN0VXJsOiAnJ1xyXG5cdFx0fSxcclxuXHRcdGZhdm9yaXRlczoge1xyXG5cdFx0XHRmZXRjaFVSTDogJ2h0dHBzOi8vanNvbnBsYWNlaG9sZGVyLnR5cGljb2RlLmNvbS9wb3N0cydcclxuXHRcdFx0Ly8gZmV0Y2hVUkw6ICdodHRwczovL2pzb25wbGFjZWhvbGRlci50eXBpY29kZS5jb20vcG9zdHMvMSdcclxuXHRcdH1cclxuXHRcdFxyXG5cdH0pLnJ1bigpO1xyXG5cclxufTtcclxuXHJcbmNvbnN0IHN0YXJ0ID0gKG9wdGlvbikgPT4ge1xyXG5cdGlmIChvcHRpb24gPT09IHRydWUpIHN0YXJ0UGFja2FnZShvcHRpb24pO1xyXG5cdGVsc2UgY29uc29sZS5lcnJvcignU3lzdGVtOiAnLCAnSSB0aGluayBzaGl0IGhhcHBlbnMg8J+YpSAnKTtcclxufTtcclxuXHJcbmNvbnN0IGFkZENzcyA9IChmaWxlTmFtZSkgPT4ge1xyXG5cdGNvbnN0IGxpbmsgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdsaW5rJyk7XHJcblx0bGluay50eXBlID0gJ3RleHQvY3NzJztcclxuXHRsaW5rLnJlbCA9ICdzdHlsZXNoZWV0JztcclxuXHRsaW5rLmhyZWYgPSBmaWxlTmFtZTtcclxuXHJcblx0ZG9jdW1lbnQuaGVhZC5hcHBlbmRDaGlsZChsaW5rKTtcclxufTtcclxuXHJcbmlmICh0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cgJiYgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIpIHtcclxuXHJcblx0aWYgKG5hdmlnYXRvci51c2VyQWdlbnQuaW5jbHVkZXMoJ0ZpcmVmb3gnKSkgYWRkQ3NzKCdjc3MvZmlyZWZveC5jc3MnKTtcclxuXHRpZiAobmF2aWdhdG9yLnVzZXJBZ2VudC5pbmNsdWRlcygnRWRnZScpKSBhZGRDc3MoJ2Nzcy9lZGdlLmNzcycpO1xyXG5cclxuXHRkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgc3RhcnQodHJ1ZSksIGZhbHNlKTtcclxufVxyXG4iLCJpbXBvcnQgJHdyYXBwZXIgZnJvbSAnLi93cmFwcGVyJztcclxuaW1wb3J0ICRyYXRpbmcgZnJvbSAnLi9yYXRpbmcnO1xyXG5pbXBvcnQgJHZpZXdQaWN0dXJlIGZyb20gJy4vdmlld1BpY3R1cmUnO1xyXG5pbXBvcnQgJGluU3RvY2sgZnJvbSAnLi9pblN0b2NrJztcclxuaW1wb3J0ICRsaXN0UHJvcGVydHkgZnJvbSAnLi9saXN0UHJvcGVydHknO1xyXG5pbXBvcnQgJGZhdm9yaXRlcyBmcm9tICcuL2Zhdm9yaXRlcyc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyAkY2FyZENvbnRyb2xsZXIge1xyXG5cdGNvbnN0cnVjdG9yKGluaXQpIHtcclxuXHRcdHRoaXMuc2VsZWN0b3IgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGluaXQuc2VsZWN0b3IpO1xyXG5cdFx0dGhpcy5zZWxlY3RvcklEID0gaW5pdC5zZWxlY3RvcjtcclxuXHRcdFxyXG5cdFx0Ly8gLS0tIEVSUk9SIC0tLVxyXG5cdFx0dGhpcy53YXJuID0gbmV4dCA9PiBjb25zb2xlLmVycm9yKCdbU1lTVEVNXTonLCBuZXh0KTtcclxuXHRcdHRoaXMuZXJyID0gdW5kZWZpbmVkO1xyXG5cdFx0aWYgKCF0aGlzLnNlbGVjdG9yKSByZXR1cm4gdGhpcy53YXJuKGBzZWxlY3RvciBpcyAke3RoaXMuZXJyfWApO1xyXG5cdFx0XHJcblx0XHQvLyAtLS0gVkFSSUFCTEVTIC0tLVxyXG5cdFx0dGhpcy5wcm9kdWN0UHJlZml4ID0gaW5pdC52aWV3UGljdHVyZS5wcm9kdWN0UHJlZml4O1xyXG5cdFx0dGhpcy5mYXZvcml0ZXNVUkwgPSBpbml0LmZhdm9yaXRlcy5mZXRjaFVSTDtcclxuXHJcblx0XHQoe1xyXG5cdFx0XHRwcmVmaXg6IHRoaXMucHJlZml4LFxyXG5cdFx0XHRmZXRjaFVSTDogdGhpcy5mZXRjaFVSTCxcclxuXHRcdFx0c2VsZWN0b3JKUzogdGhpcy5zZWxlY3RvckpTXHJcblx0XHR9ID0gaW5pdCk7XHJcblxyXG5cdFx0KHtcclxuXHRcdFx0Y291bnRTdGFyczogdGhpcy5jb3VudFN0YXJzLFxyXG5cdFx0XHRhY3RpdmVTdGFyOiB0aGlzLmFjdGl2ZVN0YXIsXHJcblx0XHRcdGRhdGFBdHRyOiB0aGlzLmRhdGFBdHRyXHJcblx0XHR9ID0gaW5pdC5yYXRpbmdTdGFycyk7XHJcblxyXG5cdFx0dGhpcy5lcnIgPSAnW1NZU1RFTV06JztcclxuXHRcdC8vIC0tLVxyXG5cdH1cclxuXHJcblx0cnVuKCkge1xyXG5cdFx0dGhpcy5mZXRjaERhdGEoKTtcclxuXHR9XHJcblxyXG5cdHJlc29sdmVCdWZmZXIoKSB7XHJcblx0XHR0aGlzLmNhcmRzTGVuZ3RoID0gNSB8fCB0aGlzLm1haW5EYXRhLlBST0RVQ1RTX1NVQ0NFU1MuZGF0YS5wcm9kdWN0cy5sZW5ndGg7XHJcblx0XHR0aGlzLmRhdGFQcm9kdWN0cyA9IHRoaXMubWFpbkRhdGEuUFJPRFVDVFNfU1VDQ0VTUy5kYXRhLnByb2R1Y3RzO1xyXG5cdFx0dGhpcy5mYXZvcml0ZUVycm9yID0gdGhpcy5tYWluRGF0YS5GQVZPUklURV9GQUlMLmRhdGEubWVzc2FnZTtcclxuXHR9XHJcblxyXG5cdHdyYXBwZXIoKSB7XHJcblx0XHRuZXcgJHdyYXBwZXIoe1xyXG5cdFx0XHRzZWxlY3RvcjogJycsXHJcblx0XHRcdGRlbGVnYXRlOiB0aGlzLnNlbGVjdG9ySUQsXHJcblx0XHRcdHByZWZpeDogdGhpcy5wcmVmaXgsXHJcblx0XHRcdGNhcmRzTGVuZ3RoOiB0aGlzLmNhcmRzTGVuZ3RoLFxyXG5cdFx0XHRwcm9kdWN0VXJsOiB0aGlzLmRhdGFQcm9kdWN0cyxcclxuXHRcdFx0c2VsZWN0b3JKUzogdGhpcy5zZWxlY3RvckpTXHJcblx0XHR9KS5ydW4oKTtcclxuXHR9XHJcblxyXG5cdHJhdGluZygpIHtcclxuXHRcdG5ldyAkcmF0aW5nKHtcclxuXHRcdFx0c2VsZWN0b3I6IGBbJHt0aGlzLnNlbGVjdG9ySlMucmF0aW5nfV1gLFxyXG5cdFx0XHRkYXRhQXR0cjogdGhpcy5kYXRhQXR0cixcclxuXHRcdFx0ZGVsZWdhdGU6IHRoaXMuc2VsZWN0b3JJRCxcclxuXHRcdFx0cHJlZml4OiB0aGlzLnByZWZpeCxcclxuXHRcdFx0Y291bnRTdGFyczogdGhpcy5jb3VudFN0YXJzLFxyXG5cdFx0XHRhY3RpdmVTdGFyOiB0aGlzLmFjdGl2ZVN0YXJcclxuXHRcdH0pLnJ1bigpO1xyXG5cdH1cclxuXHJcblx0dmlld1BpY3R1cmUoKSB7XHJcblx0XHRuZXcgJHZpZXdQaWN0dXJlKHtcclxuXHRcdFx0c2VsZWN0b3I6IGBbJHt0aGlzLnNlbGVjdG9ySlMudmlld1BpY3R1cmV9XWAsXHJcblx0XHRcdGRlbGVnYXRlOiB0aGlzLnNlbGVjdG9ySUQsXHJcblx0XHRcdHByZWZpeDogdGhpcy5wcmVmaXgsXHJcblx0XHRcdHByb2R1Y3RQcmVmaXg6IHRoaXMucHJvZHVjdFByZWZpeCxcclxuXHRcdFx0cHJvZHVjdFVybDogdGhpcy5kYXRhUHJvZHVjdHNcclxuXHRcdH0pLnJ1bigpO1xyXG5cdH1cclxuXHJcblx0aW5TdG9jaygpIHtcclxuXHRcdG5ldyAkaW5TdG9jayh7XHJcblx0XHRcdHNlbGVjdG9yOiBgWyR7dGhpcy5zZWxlY3RvckpTLmluU3RvY2t9XWAsXHJcblx0XHRcdGRlbGVnYXRlOiB0aGlzLnNlbGVjdG9ySUQsXHJcblx0XHRcdHByZWZpeDogdGhpcy5wcmVmaXgsXHJcblx0XHRcdHByb2R1Y3RVcmw6IHRoaXMuZGF0YVByb2R1Y3RzXHJcblx0XHR9KS5ydW4oKTtcclxuXHR9XHJcblxyXG5cdGxpc3RQcm9wZXJ0eSgpIHtcclxuXHRcdG5ldyAkbGlzdFByb3BlcnR5KHtcclxuXHRcdFx0c2VsZWN0b3I6IGBbJHt0aGlzLnNlbGVjdG9ySlMubGlzdFByb3BlcnR5fV1gLFxyXG5cdFx0XHRkZWxlZ2F0ZTogdGhpcy5zZWxlY3RvcklELFxyXG5cdFx0XHRwcmVmaXg6IHRoaXMucHJlZml4LFxyXG5cdFx0XHRwcm9kdWN0VXJsOiB0aGlzLmRhdGFQcm9kdWN0c1xyXG5cdFx0fSkucnVuKCk7XHJcblx0fVxyXG5cclxuXHRmYXZvcml0ZXMoKSB7XHJcblx0XHRuZXcgJGZhdm9yaXRlcyh7XHJcblx0XHRcdHNlbGVjdG9yOiBgWyR7dGhpcy5zZWxlY3RvckpTLmZhdm9yaXRlc31dYCxcclxuXHRcdFx0ZGVsZWdhdGU6IHRoaXMuc2VsZWN0b3JJRCxcclxuXHRcdFx0cHJlZml4OiB0aGlzLnByZWZpeCxcclxuXHRcdFx0cHJvZHVjdFVybDogdGhpcy5kYXRhUHJvZHVjdHMsXHJcblx0XHRcdGZldGNoVVJMOiB0aGlzLmZhdm9yaXRlc1VSTCxcclxuXHRcdFx0ZXJyb3JNZXNzYWdlOiB0aGlzLmZhdm9yaXRlRXJyb3JcclxuXHRcdH0pLnJ1bigpO1xyXG5cdH1cclxuXHJcblx0bG9nKCkge1xyXG5cdFx0Y29uc29sZS5sb2codGhpcy5tYWluRGF0YSk7XHJcblx0XHRkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjbG9hZGVyJykuY2xhc3NMaXN0LmFkZCgnREVMRVRFJyk7XHJcblx0fVxyXG5cclxuXHRmZXRjaERhdGEoKSB7XHJcblx0XHRjb25zdCBvcHRpb25zID0ge1xyXG5cdFx0XHRtZXRob2Q6ICdHRVQnLFxyXG5cdFx0XHRoZWFkZXJzOiB7IEFjY2VwdDogJ2FwcGxpY2F0aW9uL2pzb24sIHRleHQvcGxhaW4sICovKicsICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicgfSxcclxuXHRcdFx0bW9kZTogJ2NvcnMnLFxyXG5cdFx0XHRjYWNoZTogJ2RlZmF1bHQnXHJcblx0XHR9O1xyXG5cclxuXHRcdGNvbnN0IHVybCA9IHRoaXMuZmV0Y2hVUkw7XHJcblxyXG5cdFx0ZmV0Y2godXJsLCBvcHRpb25zKVxyXG5cdFx0XHQudGhlbihvblJlc29sdmUgPT4gb25SZXNvbHZlLmpzb24oKSlcclxuXHRcdFx0LnRoZW4oZGF0YSA9PiAodGhpcy5tYWluRGF0YSA9IGRhdGEpKVxyXG5cdFx0XHQudGhlbih0aGlzLnJlc29sdmVCdWZmZXIuYmluZCh0aGlzKSlcclxuXHRcdFx0LnRoZW4odGhpcy53cmFwcGVyLmJpbmQodGhpcykpXHJcblx0XHRcdC50aGVuKHRoaXMucmF0aW5nLmJpbmQodGhpcykpXHJcblx0XHRcdC50aGVuKHRoaXMudmlld1BpY3R1cmUuYmluZCh0aGlzKSlcclxuXHRcdFx0LnRoZW4odGhpcy5pblN0b2NrLmJpbmQodGhpcykpXHJcblx0XHRcdC50aGVuKHRoaXMubGlzdFByb3BlcnR5LmJpbmQodGhpcykpXHJcblx0XHRcdC50aGVuKHRoaXMuZmF2b3JpdGVzLmJpbmQodGhpcykpXHJcblx0XHRcdC50aGVuKHRoaXMubG9nLmJpbmQodGhpcykpXHJcblx0XHRcdC8vIC5jYXRjaChvblJlamVjdGVkID0+IGNvbnNvbGUuZXJyb3IodGhpcy5lcnIsIGAke29uUmVqZWN0ZWQubWVzc2FnZX1gKSk7XHJcblx0fVxyXG59XHJcbiIsImltcG9ydCAkcmVuZGVyIGZyb20gJy4vcmVuZGVyJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzICRpbmRleCB7XHJcblx0Y29uc3RydWN0b3IoaW5pdCkge1xyXG5cdFx0dGhpcy5zZWxlY3RvciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoaW5pdC5zZWxlY3Rvcik7XHJcblx0XHR0aGlzLnNlbGVjdG9ySUQgPSBpbml0LnNlbGVjdG9yO1xyXG5cdFx0aWYgKCF0aGlzLnNlbGVjdG9yKSByZXR1cm47XHJcblxyXG5cdFx0Ly8gLS0tIFZBUklBQkxFUyAtLS1cclxuXHRcdHRoaXMuYXJyQ29sbGVjdGlvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwodGhpcy5zZWxlY3RvcklEKTtcclxuXHRcdHRoaXMuc2VsZWN0b3JQYXJlbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGluaXQuZGVsZWdhdGUpO1xyXG5cdFx0dGhpcy5wcmVmaXggPSBpbml0LnByZWZpeDtcclxuXHJcblx0XHR0aGlzLnByb2R1Y3RVcmwgPSBpbml0LnByb2R1Y3RVcmw7XHJcblx0XHR0aGlzLnByb2R1Y3RUZXh0ID0gaW5pdC5wcm9kdWN0VGV4dDtcclxuXHRcdHRoaXMuZmV0Y2hVUkwgPSBpbml0LmZldGNoVVJMO1xyXG5cdFx0dGhpcy5lcnJvck1lc3NhZ2UgPSBpbml0LmVycm9yTWVzc2FnZTtcclxuXHRcdHRoaXMuZXJyID0gdW5kZWZpbmVkO1xyXG5cdFx0Ly8gLS0tXHJcblx0fVxyXG5cclxuXHRydW4oKSB7XHJcblx0XHRpZiAodGhpcy5zZWxlY3Rvcikge1xyXG5cdFx0XHR0aGlzLmZpcnN0UnVuKCk7XHJcblx0XHRcdHRoaXMuZXZlbnRIYW5kbGVycygpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0ZXZlbnRIYW5kbGVycygpIHtcclxuXHRcdHRoaXMuc2VsZWN0b3JQYXJlbnQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBldmVudCA9PiB0aGlzLmZhdm9ySGVhcnQoZXZlbnQpLCBmYWxzZSk7XHJcblx0XHR0aGlzLnNlbGVjdG9yUGFyZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2FuaW1hdGlvbmVuZCcsIGV2ZW50ID0+IHRoaXMuYW5pbWF0ZUVuZChldmVudCksIGZhbHNlKTtcclxuXHR9XHJcblxyXG5cdGZpcnN0UnVuKCkge1xyXG5cdFx0dGhpcy5tYWluKCk7XHJcblx0fVxyXG5cclxuXHRfX2J1aWxkUE9TVCgpIHtcclxuXHRcdHRoaXMub2JqID0ge1xyXG5cdFx0XHRwcm9kdWN0czoge1xyXG5cdFx0XHRcdGlkOiBOdW1iZXIodGhpcy5pZC5nZXRBdHRyaWJ1dGUoJ2RhdGEtc3RvY2staWQnKSksXHJcblx0XHRcdFx0aW5GYXY6IEJvb2xlYW4odGhpcy5jdXJzb3IuZ2V0QXR0cmlidXRlKCdkYXRhLWluLWZhdm9yaXRlJykudG9Mb3dlckNhc2UoKSA9PT0gJ2ZhbHNlJylcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHRcdHJldHVybiBjb25zb2xlLmxvZygndGhpcy5vYmonLCB0aGlzLm9iaik7XHJcblx0fVxyXG5cclxuXHRfX3N3aXRjaFJlc3VsdChldmVudCkge1xyXG5cdFx0Y29uc29sZS5sb2coJ3RoaXMuY2xvc2VzdCcsIHRoaXMuY2xvc2VzdCk7XHJcblx0XHRjb25zb2xlLmxvZygndGhpcy5pZCcsIHRoaXMuaWQuZ2V0QXR0cmlidXRlKCdkYXRhLXN0b2NrLWlkJykpO1xyXG5cdFx0Wy4uLnRoaXMuY2xvc2VzdC5xdWVyeVNlbGVjdG9yQWxsKCdzcGFuJyldLmZvckVhY2goZiA9PiBmLmNsYXNzTGlzdC50b2dnbGUoJ2hpZGRlbicpKTtcclxuXHR9XHJcblxyXG5cdF9fYW5pbWF0ZVN0YXJ0KCkge1xyXG5cdFx0dGhpcy5jdXJzb3IuY2xhc3NMaXN0LmFkZCgnYW5pbWEnKTtcclxuXHRcdHRoaXMuX19zd2l0Y2hSZXN1bHQoKTtcclxuXHR9XHJcblxyXG5cdGFuaW1hdGVFbmQoZXZlbnQpIHtcclxuXHRcdHRoaXMuY3Vyc29yLmNsYXNzTGlzdC5yZW1vdmUoJ2FuaW1hJyk7XHJcblx0XHRpZiAodGhpcy5jdXJzb3IuZ2V0QXR0cmlidXRlKCdkYXRhLWluLWZhdm9yaXRlJykgPT09ICdmYWxzZScpIHRoaXMuY3Vyc29yLnNldEF0dHJpYnV0ZSgnZGF0YS1pbi1mYXZvcml0ZScsIHRydWUpO1xyXG5cdFx0ZWxzZSB0aGlzLmN1cnNvci5zZXRBdHRyaWJ1dGUoJ2RhdGEtaW4tZmF2b3JpdGUnLCBmYWxzZSk7XHJcblx0fVxyXG5cclxuXHRmZXRjaERhdGEoc2VuZERhdGEpIHtcclxuXHRcdHRoaXMudXJsID0gdGhpcy5mZXRjaFVSTDtcclxuXHRcdHRoaXMub3B0aW9ucyA9IHtcclxuXHRcdFx0bWV0aG9kOiAnUE9TVCcsXHJcblx0XHRcdG1vZGU6ICdjb3JzJyxcclxuXHRcdFx0aGVhZGVyczogeyAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nIH0sXHJcblx0XHRcdGJvZHk6IEpTT04uc3RyaW5naWZ5KHNlbmREYXRhKVxyXG5cdFx0fTtcclxuXHJcblx0XHRmZXRjaCh0aGlzLnVybCwgdGhpcy5vcHRpb25zKVxyXG5cdFx0XHQudGhlbigob25SZXNvbHZlKSA9PiB7XHJcblx0XHRcdFx0dGhpcy5zdGF0dXMgPSBvblJlc29sdmUuc3RhdHVzO1xyXG5cdFx0XHRcdHJldHVybiBvblJlc29sdmUuanNvbigpO1xyXG5cdFx0XHR9KVxyXG5cdFx0XHQudGhlbihkYXRhID0+ICh0aGlzLm9iai5wcm9kdWN0cy5pZCAhPT0gMiA/IEpTT04uc3RyaW5naWZ5KGRhdGEpIDogUHJvbWlzZS5yZWplY3QoZGF0YSkpKVxyXG5cdFx0XHQvLyAudGhlbihkYXRhID0+ICh0aGlzLnN0YXR1cyA9PT0gMjAxID8gSlNPTi5zdHJpbmdpZnkoZGF0YSkgOiBQcm9taXNlLnJlamVjdChkYXRhKSkpXHJcblx0XHRcdC50aGVuKCgpID0+IHRoaXMuX19hbmltYXRlU3RhcnQoKSlcclxuXHRcdFx0LmNhdGNoKG9uUmVqZWN0ZWQgPT4gY29uc29sZS5lcnJvcih0aGlzLmVycm9yTWVzc2FnZSkpO1xyXG5cdH1cclxuXHJcblx0ZmF2b3JIZWFydChldmVudCkge1xyXG5cdFx0dGhpcy5ldmVudCA9IGV2ZW50LnRhcmdldDtcclxuXHRcdHRoaXMuY2xvc2VzdCA9IHRoaXMuZXZlbnQuY2xvc2VzdCh0aGlzLnNlbGVjdG9ySUQpO1xyXG5cdFx0dGhpcy5pZCA9IHRoaXMuZXZlbnQuY2xvc2VzdCgnW2RhdGEtc3RvY2staWRdJyk7XHJcblxyXG5cdFx0aWYgKHRoaXMuY2xvc2VzdCAmJiB0aGlzLmV2ZW50Lm1hdGNoZXMoYC4ke3RoaXMucHJlZml4fV9fc3ZnLS1oZWFydGApKSB7XHJcblx0XHRcdGlmICh0aGlzLmlkLmdldEF0dHJpYnV0ZSgnZGF0YS1zdG9jay1pZCcpID09PSAndW5kZWZpbmVkJykgcmV0dXJuO1xyXG5cdFx0XHR0aGlzLmN1cnNvciA9IHRoaXMuY2xvc2VzdC5xdWVyeVNlbGVjdG9yKGAuJHt0aGlzLnByZWZpeH1fX2N1cnNvcmApO1xyXG5cclxuXHRcdFx0dGhpcy5fX2J1aWxkUE9TVCgpO1xyXG5cdFx0XHR0aGlzLmZldGNoRGF0YSh0aGlzLm9iaik7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRfX3ZhbGlkKHByb3AsIG5leHRwcm9wLCBpbmRleCkge1xyXG5cdFx0cmV0dXJuIChwcm9wW2luZGV4XSAmJiBwcm9wW2luZGV4XVtuZXh0cHJvcF0pID8gcHJvcFtpbmRleF1bbmV4dHByb3BdIDogdGhpcy5lcnI7XHJcblx0fVxyXG5cclxuXHRfX3JlbmRlcihmLCBpbmRleCkge1xyXG5cdFx0dGhpcy5yZW5kZXIgPSBuZXcgJHJlbmRlcih7XHJcblx0XHRcdHByZWZpeDogdGhpcy5wcmVmaXgsXHJcblx0XHRcdHNlbGVjdG9yOiBmLFxyXG5cdFx0XHRwcm9kdWN0RmF2OiB0aGlzLl9fdmFsaWQodGhpcy5wcm9kdWN0VXJsLCAnaW5GYXYnLCBpbmRleClcclxuXHRcdH0pO1xyXG5cdH1cclxuXHJcblx0bWFpbigpIHtcclxuXHRcdEFycmF5LmZyb20odGhpcy5hcnJDb2xsZWN0aW9uLCAoZiwgaW5kZXgpID0+IHRoaXMuX19yZW5kZXIoZiwgaW5kZXgpKTtcclxuXHR9XHJcbn1cclxuIiwiZXhwb3J0IGRlZmF1bHQgY2xhc3MgJHJlbmRlciB7XHJcblx0Y29uc3RydWN0b3IoaW5pdCkge1xyXG5cdFx0Ly8gLS0tIFZBUklBQkxFUyAtLS1cclxuXHRcdHRoaXMucHJlZml4ID0gaW5pdC5wcmVmaXg7XHJcblx0XHR0aGlzLnNlbGVjdG9yID0gaW5pdC5zZWxlY3RvcjtcclxuXHRcdHRoaXMucHJvZHVjdEZhdiA9IGluaXQucHJvZHVjdEZhdiB8fCBmYWxzZTtcclxuXHRcdHRoaXMuZXJyID0gdW5kZWZpbmVkO1xyXG5cdFx0Ly8gLS0tXHJcblx0XHR0aGlzLnJlbmRlcigpO1xyXG5cdH1cclxuXHRcclxuXHRfX3JlbmRlcigpIHtcclxuXHRcdFxyXG5cdFx0Y29uc3QgZWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcclxuXHRcdGVsLmNsYXNzTGlzdCA9IGAke3RoaXMucHJlZml4fV9fbGlzdC1pdGVtICR7dGhpcy5wcmVmaXh9X19jdXJzb3JgO1xyXG5cdFx0ZWwuZGF0YXNldC5pbkZhdm9yaXRlID0gYCR7dGhpcy5wcm9kdWN0RmF2fWA7XHJcblx0XHRlbC5pbm5lckhUTUwgPSBgXHJcblx0XHQ8c3BhbiBjbGFzcz1cIiR7dGhpcy5wcmVmaXh9X19zdmcgJHt0aGlzLnByZWZpeH1fX3JlcGFpbiAke3RoaXMucHJlZml4fV9fc3ZnLS1oZWFydFwiPjxzdmcgcm9sZT1cInBpY3R1cmUtc3ZnXCIgY2xhc3M9XCJnbHlwaHNfX2hlYXJ0XCI+XHJcblx0XHQ8dXNlIHhsaW5rOmhyZWY9XCIjaWQtZ2x5cGhzLWhlYXJ0XCI+PC91c2U+XHJcblx0XHQ8L3N2Zz48L3NwYW4+XHJcblx0XHQ8c3BhbiBjbGFzcz1cIiR7dGhpcy5wcmVmaXh9X19zdmcgJHt0aGlzLnByZWZpeH1fX3JlcGFpbiAke3RoaXMucHJlZml4fV9fc3ZnLS1oZWFydCBoaWRkZW5cIj48c3ZnIHJvbGU9XCJwaWN0dXJlLXN2Z1wiIGNsYXNzPVwiZ2x5cGhzX19oZWFydC1mdWxsXCI+XHJcblx0XHQ8dXNlIHhsaW5rOmhyZWY9XCIjaWQtZ2x5cGhzLWhlYXJ0LWZ1bGxcIj48L3VzZT5cclxuXHRcdDwvc3ZnPjwvc3Bhbj5cclxuXHRcdGA7XHJcblx0XHRyZXR1cm4gdGhpcy5zZWxlY3Rvci5hcHBlbmRDaGlsZChlbCk7XHJcblx0XHRcclxuXHR9XHJcblx0XHJcblx0X19zdGF0dXMoKSB7XHJcblx0XHRpZiAodGhpcy5wcm9kdWN0RmF2KSB7XHJcblx0XHRcdFsuLi50aGlzLnNlbGVjdG9yLnF1ZXJ5U2VsZWN0b3JBbGwoJ3NwYW4nKV0uZm9yRWFjaChmID0+IGYuY2xhc3NMaXN0LnRvZ2dsZSgnaGlkZGVuJykpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0cmVuZGVyKCkge1xyXG5cdFx0dGhpcy5fX3JlbmRlcigpO1xyXG5cdFx0dGhpcy5fX3N0YXR1cygpO1xyXG5cdH1cclxufVxyXG4iLCJpbXBvcnQgJHJlbmRlciBmcm9tICcuL3JlbmRlcic7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyAkaW5kZXgge1xyXG5cdGNvbnN0cnVjdG9yKGluaXQpIHtcclxuXHRcdHRoaXMuc2VsZWN0b3IgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGluaXQuc2VsZWN0b3IpO1xyXG5cdFx0dGhpcy5zZWxlY3RvcklEID0gaW5pdC5zZWxlY3RvcjtcclxuXHRcdGlmICghdGhpcy5zZWxlY3RvcikgcmV0dXJuO1xyXG5cclxuXHRcdC8vIC0tLSBWQVJJQUJMRVMgLS0tXHJcblx0XHR0aGlzLmFyckNvbGxlY3Rpb24gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKHRoaXMuc2VsZWN0b3JJRCk7XHJcblx0XHR0aGlzLnByZWZpeCA9IGluaXQucHJlZml4O1xyXG5cclxuXHRcdHRoaXMucHJvZHVjdFVybCA9IGluaXQucHJvZHVjdFVybDtcclxuXHRcdHRoaXMucHJvZHVjdFRleHQgPSBpbml0LnByb2R1Y3RUZXh0O1xyXG5cdFx0dGhpcy5lcnIgPSB1bmRlZmluZWQ7XHJcblx0XHQvLyAtLS1cclxuXHR9XHJcblxyXG5cdHJ1bigpIHtcclxuXHRcdGlmICh0aGlzLnNlbGVjdG9yKSB7XHJcblx0XHRcdHRoaXMuZmlyc3RSdW4oKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdGZpcnN0UnVuKCkge1xyXG5cdFx0dGhpcy5tYWluKCk7XHJcblx0fVxyXG5cclxuXHRfX3ZhbGlkKHByb3AsIG5leHRwcm9wLCBpbmRleCkge1xyXG5cdFx0cmV0dXJuIChwcm9wW2luZGV4XSAmJiBwcm9wW2luZGV4XVtuZXh0cHJvcF0pID8gcHJvcFtpbmRleF1bbmV4dHByb3BdIDogdGhpcy5lcnI7XHJcblx0fVxyXG5cclxuXHRfX3JlbmRlcihmLCBpbmRleCkge1xyXG5cdFx0dGhpcy5yZW5kZXIgPSBuZXcgJHJlbmRlcih7XHJcblx0XHRcdHByZWZpeDogdGhpcy5wcmVmaXgsXHJcblx0XHRcdHNlbGVjdG9yOiBmLFxyXG5cdFx0XHRwcm9kdWN0VXJsOiB0aGlzLl9fdmFsaWQodGhpcy5wcm9kdWN0VXJsLCAnYXZhaWxhYmlsaXR5JywgaW5kZXgpLFxyXG5cdFx0XHRwcm9kdWN0VGV4dDogdGhpcy5fX3ZhbGlkKHRoaXMucHJvZHVjdFVybCwgJ3RpdGxlJywgaW5kZXgpXHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG5cdG1haW4oKSB7XHJcblx0XHRBcnJheS5mcm9tKHRoaXMuYXJyQ29sbGVjdGlvbiwgKGYsIGluZGV4KSA9PiB0aGlzLl9fcmVuZGVyKGYsIGluZGV4KSk7XHJcblx0fVxyXG59XHJcbiIsImV4cG9ydCBkZWZhdWx0IGNsYXNzICRyZW5kZXIge1xyXG5cdGNvbnN0cnVjdG9yKGluaXQpIHtcclxuXHRcdC8vIC0tLSBWQVJJQUJMRVMgLS0tXHJcblx0XHR0aGlzLnByZWZpeCA9IGluaXQucHJlZml4O1xyXG5cdFx0dGhpcy5zZWxlY3RvciA9IGluaXQuc2VsZWN0b3I7XHJcblxyXG5cdFx0dGhpcy5zdGF0dXMgPSBpbml0LnByb2R1Y3RVcmw7XHJcblx0XHR0aGlzLnByb2R1Y3RUZXh0ID0gaW5pdC5wcm9kdWN0VGV4dDtcclxuXHRcdC8vIC0tLVxyXG5cdFx0dGhpcy5tYWluKCk7XHJcblx0fVxyXG5cclxuXHRfX3JlbmRlcigpIHtcclxuXHJcblx0XHR0aGlzLnJlc3VsdCA9IHtcclxuXHRcdFx0c3VjY2VzczogYFxyXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiJHt0aGlzLnByZWZpeH1fX2l0ZW0gJHt0aGlzLnByZWZpeH1fX2luIHN1Y2Nlc3NcIj5cclxuXHRcdFx0PHNwYW4gY2xhc3M9XCIke3RoaXMucHJlZml4fV9fc3ZnICR7dGhpcy5wcmVmaXh9X19zdmctLWNoZWNrZWRcIj48c3ZnIHJvbGU9XCJwaWN0dXJlLXN2Z1wiIGNsYXNzPVwiZ2x5cGhzX19jaGVja2VkXCI+XHJcblx0XHRcdDx1c2UgeGxpbms6aHJlZj1cIiNpZC1nbHlwaHMtY2hlY2tlZFwiPjwvdXNlPlxyXG5cdFx0XHQ8L3N2Zz48L3NwYW4+XHJcblx0XHRcdDxzcGFuPtCSINC90LDQu9C40YfQuNC4PC9zcGFuPlxyXG5cdFx0XHQ8L2Rpdj5cclxuXHRcdFx0YCxcclxuXHRcdFx0ZXJyb3I6IGBcclxuXHRcdFx0PGRpdiBjbGFzcz1cIiR7dGhpcy5wcmVmaXh9X19pdGVtICR7dGhpcy5wcmVmaXh9X19pbiBlcnJvclwiPlxyXG5cdFx0XHQ8c3BhbiBjbGFzcz1cIiR7dGhpcy5wcmVmaXh9X19zdmcgJHt0aGlzLnByZWZpeH1fX3N2Zy0tY2hlY2tlZFwiPjxzdmcgcm9sZT1cInBpY3R1cmUtc3ZnXCIgY2xhc3M9XCJnbHlwaHNfX3VuY2hlY2tlZFwiPlxyXG5cdFx0XHQ8dXNlIHhsaW5rOmhyZWY9XCIjaWQtZ2x5cGhzLXVuY2hlY2tlZFwiPjwvdXNlPlxyXG5cdFx0XHQ8L3N2Zz48L3NwYW4+XHJcblx0XHRcdDxzcGFuPtCd0LXRgiDQsiDQvdCw0LvQuNGH0LjQuDwvc3Bhbj5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHRcdGBcclxuXHRcdH07XHJcblxyXG5cdFx0Y29uc3QgZWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcclxuXHRcdGVsLmNsYXNzTGlzdCA9IGAke3RoaXMucHJlZml4fV9fZ3JvdXAgJHt0aGlzLnByZWZpeH1fX25hbWVgO1xyXG5cdFx0ZWwuaW5uZXJIVE1MID0gYFxyXG5cdFx0XHQkeyghdGhpcy5zdGF0dXMpID8gdGhpcy5yZXN1bHQuZXJyb3IgOiB0aGlzLnJlc3VsdC5zdWNjZXNzfVxyXG5cdFx0XHQ8aDQgY2xhc3M9XCIke3RoaXMucHJlZml4fV9faXRlbSAke3RoaXMucHJlZml4fV9faDRcIj4ke3RoaXMucHJvZHVjdFRleHR9PC9oND5cclxuXHRcdGA7XHJcblx0XHR0aGlzLnNlbGVjdG9yLnByZXBlbmQoZWwpO1xyXG5cdH1cclxuXHJcblx0bWFpbigpIHtcclxuXHRcdHRoaXMuX19yZW5kZXIoKTtcclxuXHR9XHJcbn1cclxuIiwiaW1wb3J0ICRyZW5kZXIgZnJvbSAnLi9yZW5kZXInO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgJGluZGV4IHtcclxuXHRjb25zdHJ1Y3Rvcihpbml0KSB7XHJcblx0XHR0aGlzLnNlbGVjdG9yID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcihpbml0LnNlbGVjdG9yKTtcclxuXHRcdHRoaXMuc2VsZWN0b3JJRCA9IGluaXQuc2VsZWN0b3I7XHJcblx0XHRpZiAoIXRoaXMuc2VsZWN0b3IpIHJldHVybjtcclxuXHJcblx0XHQvLyAtLS0gVkFSSUFCTEVTIC0tLVxyXG5cdFx0dGhpcy5hcnJDb2xsZWN0aW9uID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCh0aGlzLnNlbGVjdG9ySUQpO1xyXG5cdFx0dGhpcy5wcmVmaXggPSBpbml0LnByZWZpeDtcclxuXHJcblx0XHR0aGlzLnByb2R1Y3RVcmwgPSBpbml0LnByb2R1Y3RVcmw7XHJcblx0XHR0aGlzLnByb2R1Y3RUZXh0ID0gaW5pdC5wcm9kdWN0VGV4dDtcclxuXHRcdHRoaXMuZXJyID0gdW5kZWZpbmVkO1xyXG5cdFx0Ly8gLS0tXHJcblx0fVxyXG5cclxuXHRydW4oKSB7XHJcblx0XHRpZiAodGhpcy5zZWxlY3Rvcikge1xyXG5cdFx0XHR0aGlzLmZpcnN0UnVuKCk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRmaXJzdFJ1bigpIHtcclxuXHRcdHRoaXMubWFpbigpO1xyXG5cdH1cclxuXHJcblx0X192YWxpZChwcm9wLCBuZXh0cHJvcCwgaW5kZXgpIHtcclxuXHRcdHJldHVybiAocHJvcFtpbmRleF0gJiYgcHJvcFtpbmRleF1bbmV4dHByb3BdKSA/IHByb3BbaW5kZXhdW25leHRwcm9wXSA6IHRoaXMuZXJyO1xyXG5cdH1cclxuXHJcblx0X19yZW5kZXIoZiwgaW5kZXgpIHtcclxuXHRcdHRoaXMucmVuZGVyID0gbmV3ICRyZW5kZXIoe1xyXG5cdFx0XHRwcmVmaXg6IHRoaXMucHJlZml4LFxyXG5cdFx0XHRzZWxlY3RvcjogZixcclxuXHRcdFx0cHJvZHVjdExpc3Q6IHRoaXMuX192YWxpZCh0aGlzLnByb2R1Y3RVcmwsICdwYXJhbXMnLCBpbmRleClcclxuXHRcdH0pO1xyXG5cdH1cclxuXHJcblx0bWFpbigpIHtcclxuXHRcdEFycmF5LmZyb20odGhpcy5hcnJDb2xsZWN0aW9uLCAoZiwgaW5kZXgpID0+IHRoaXMuX19yZW5kZXIoZiwgaW5kZXgpKTtcclxuXHR9XHJcbn1cclxuIiwiZXhwb3J0IGRlZmF1bHQgY2xhc3MgJHJlbmRlciB7XHJcblx0Y29uc3RydWN0b3IoaW5pdCkge1xyXG5cdFx0Ly8gLS0tIFZBUklBQkxFUyAtLS1cclxuXHRcdHRoaXMucHJlZml4ID0gaW5pdC5wcmVmaXg7XHJcblx0XHR0aGlzLnNlbGVjdG9yID0gaW5pdC5zZWxlY3RvcjtcclxuXHJcblx0XHR0aGlzLnN0YXR1cyA9IGluaXQucHJvZHVjdFVybDtcclxuXHRcdHRoaXMucHJvZHVjdExpc3QgPSBpbml0LnByb2R1Y3RMaXN0O1xyXG5cdFx0dGhpcy5lcnIgPSB1bmRlZmluZWQ7XHJcblx0XHQvLyAtLS1cclxuXHRcdHRoaXMubWFpbigpO1xyXG5cdH1cclxuXHJcblx0X192YWxpZChwcm9wLCBuZXh0cHJvcCwgaW5kZXgpIHtcclxuXHRcdHJldHVybiAocHJvcFtpbmRleF0gJiYgcHJvcFtpbmRleF1bbmV4dHByb3BdKSA/IHByb3BbaW5kZXhdW25leHRwcm9wXSA6IHRoaXMuZXJyO1xyXG5cdH1cclxuXHRcclxuXHRfX3dhcm5pbmcoKSB7XHJcblx0XHRpZiAoIXRoaXMucHJvZHVjdExpc3QpIHRoaXMucHJvZHVjdExpc3QgPSBbeyAwOiB0aGlzLmVyciB9XTtcclxuXHR9XHJcblx0XHJcblx0X19yZW5kZXIoKSB7XHJcblx0XHRcclxuXHRcdHRoaXMucHJvZHVjdExpc3QubWFwKChmLCBpbmRleCkgPT4ge1xyXG5cdFx0XHRjb25zdCBlbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG5cdFx0XHRlbC5jbGFzc0xpc3QgPSBgJHt0aGlzLnByZWZpeH1fX2xpc3QtaXRlbWA7XHJcblx0XHRcdGVsLmlubmVySFRNTCA9IGBcclxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwic3RvY2tfX2xpc3QtZWxcIj4ke3RoaXMuX192YWxpZCh0aGlzLnByb2R1Y3RMaXN0LCAnbmFtZScsIGluZGV4KX08L2Rpdj5cclxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwic3RvY2tfX2xpc3QtZWxcIj4ke3RoaXMuX192YWxpZCh0aGlzLnByb2R1Y3RMaXN0LCAndmFsdWUnLCBpbmRleCl9PC9kaXY+XHJcblx0XHRcdGA7XHJcblx0XHRcdHJldHVybiB0aGlzLnNlbGVjdG9yLmFwcGVuZENoaWxkKGVsKTtcclxuXHRcdH0pO1xyXG5cclxuXHR9XHJcblxyXG5cdG1haW4oKSB7XHJcblx0XHR0aGlzLl9fd2FybmluZygpO1xyXG5cdFx0dGhpcy5fX3JlbmRlcigpO1xyXG5cdH1cclxufVxyXG4iLCJpbXBvcnQgJHJlbmRlciBmcm9tICcuL3JlbmRlcic7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyAkaW5kZXgge1xyXG5cdGNvbnN0cnVjdG9yKGluaXQpIHtcclxuXHRcdCh7XHJcblx0XHRcdHNlbGVjdG9yOiB0aGlzLnNlbGVjdG9yLFxyXG5cdFx0XHRkZWxlZ2F0ZTogdGhpcy5kZWxlZ2F0ZSxcclxuXHRcdFx0ZGF0YUF0dHI6IHRoaXMuZGF0YUF0dHIsXHJcblx0XHRcdHByZWZpeDogdGhpcy5wcmVmaXgsXHJcblx0XHRcdGFjdGl2ZVN0YXI6IHRoaXMuYWN0aXZlU3RhcixcclxuXHRcdFx0Y291bnRTdGFyczogdGhpcy5jb3VudFN0YXJzXHJcblx0XHR9ID0gaW5pdCk7XHJcblx0XHRjb25zb2xlLmxvZygnaW5pdCcsIGluaXQpO1xyXG5cdFx0XHJcblx0XHQvLyAtLS0gRVJST1IgLS0tXHJcblx0XHR0aGlzLmVyciA9IHVuZGVmaW5lZDtcclxuXHRcdHRoaXMud2FybiA9IGVyciA9PiBjb25zb2xlLmVycm9yKCdbU1lTVEVNXTonLCBlcnIpO1xyXG5cdFx0aWYgKCF0aGlzLnNlbGVjdG9yKSByZXR1cm4gdGhpcy53YXJuKGBzZWxlY3RvciBpcyAke3RoaXMuZXJyfWApO1xyXG5cclxuXHRcdC8vIC0tLSBWQVJJQUJMRVMgLS0tXHJcblx0XHR0aGlzLnNlbGVjdG9yUGFyZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0aGlzLmRlbGVnYXRlKTtcclxuXHRcdHRoaXMuYXJyQ29sbGVjdGlvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwodGhpcy5zZWxlY3Rvcik7XHJcblx0XHR0aGlzLnN1bVN0YXJzID0gKHRoaXMuY291bnRTdGFycyA+PSAxICYmIHRoaXMuY291bnRTdGFycyA8PSA5KSA/IHRoaXMuY291bnRTdGFycyA6IHRoaXMud2FybihgY291bnRTdGFycyBpcyAke3RoaXMuZXJyfWApO1xyXG5cdFx0dGhpcy5zdW1BY3RpdmUgPSAodGhpcy5hY3RpdmVTdGFyID49IDAgJiYgdGhpcy5hY3RpdmVTdGFyIDw9IHRoaXMuY291bnRTdGFycykgPyB0aGlzLmFjdGl2ZVN0YXIgOiB0aGlzLndhcm4oYGFjdGl2ZVN0YXIgaXMgJHt0aGlzLmVycn1gKTtcclxuXHR9XHJcblxyXG5cdHJ1bigpIHtcclxuXHRcdHRoaXMuZmlyc3RSdW4oKTtcclxuXHRcdHRoaXMuZXZlbnRIYW5kbGVycygpO1xyXG5cdH1cclxuXHJcblx0ZXZlbnRIYW5kbGVycygpIHtcclxuXHRcdHRoaXMuc2VsZWN0b3JQYXJlbnQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBldmVudCA9PiB0aGlzLl9fZGVsZWdhdGVIYW5kbGVyKGV2ZW50KSwgZmFsc2UpO1xyXG5cdH1cclxuXHJcblx0Zmlyc3RSdW4oKSB7XHJcblx0XHR0aGlzLnJlbmRlcigpO1xyXG5cdH1cclxuXHJcblx0cmVuZGVyKCkge1xyXG5cdFx0QXJyYXkuZnJvbSh0aGlzLmFyckNvbGxlY3Rpb24sIGYgPT4gbmV3ICRyZW5kZXIoe1xyXG5cdFx0XHRwcmVmaXg6IHRoaXMucHJlZml4LFxyXG5cdFx0XHRzZWxlY3RvcjogZixcclxuXHRcdFx0Y291bnRTdGFyczogdGhpcy5zdW1TdGFycyxcclxuXHRcdFx0YWN0aXZlU3RhcjogdGhpcy5zdW1BY3RpdmVcclxuXHRcdH0pKTtcclxuXHR9XHJcblxyXG5cdF9fZGVsZWdhdGVIYW5kbGVyKGV2ZW50KSB7XHJcblx0XHR0aGlzLmV2ZW50ID0gZXZlbnQudGFyZ2V0O1xyXG5cdFx0dGhpcy5jbG9zZXN0ID0gdGhpcy5ldmVudC5jbG9zZXN0KHRoaXMuc2VsZWN0b3IpO1xyXG5cdFx0dGhpcy5jbGVhbkFsbCA9ICgpID0+IFsuLi50aGlzLmNsb3Nlc3QuY2hpbGRyZW5dXHJcblx0XHRcdC5maWx0ZXIoZiA9PiBmLmNsYXNzTGlzdC5jb250YWlucygnYWN0aXZlJykpXHJcblx0XHRcdC5mb3JFYWNoKGYgPT4gZi5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKSk7XHJcblxyXG5cdFx0aWYgKHRoaXMuY2xvc2VzdCAmJiB0aGlzLmV2ZW50Lm1hdGNoZXMoYC4ke3RoaXMucHJlZml4fV9fc3RhcnMtZWxgKSkge1xyXG5cdFx0XHRpZiAodGhpcy5ldmVudC5jbGFzc0xpc3QuY29udGFpbnMoJ2FjdGl2ZScpKSB7XHJcblx0XHRcdFx0dGhpcy5jbGVhbkFsbCgpO1xyXG5cdFx0XHRcdHRoaXMuY2xvc2VzdC5zZXRBdHRyaWJ1dGUodGhpcy5kYXRhQXR0ciwgMCk7XHJcblx0XHRcdFx0dGhpcy5jbG9zZXN0LmZpcnN0RWxlbWVudENoaWxkLmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpO1xyXG5cdFx0XHRcdHJldHVybjtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0dGhpcy5zdGFySW5kZXggPSBbLi4udGhpcy5ldmVudC5wYXJlbnRFbGVtZW50LmNoaWxkcmVuXS5pbmRleE9mKHRoaXMuZXZlbnQpO1xyXG5cdFx0XHR0aGlzLmNsb3Nlc3QuZ2V0QXR0cmlidXRlKHRoaXMuZGF0YUF0dHIpO1xyXG5cdFx0XHR0aGlzLmNsb3Nlc3Quc2V0QXR0cmlidXRlKHRoaXMuZGF0YUF0dHIsIHRoaXMuc3RhckluZGV4KTtcclxuXHJcblx0XHRcdHRoaXMuY2xlYW5BbGwoKTtcclxuXHRcdFx0dGhpcy5ldmVudC5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKTtcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuIiwiZXhwb3J0IGRlZmF1bHQgY2xhc3MgJHJlbmRlciB7XHJcblx0Y29uc3RydWN0b3IoaW5pdCkge1xyXG5cdFx0Ly8gLS0tIFZBUklBQkxFUyAtLS1cclxuXHRcdHRoaXMucHJlZml4ID0gaW5pdC5wcmVmaXg7XHJcblx0XHR0aGlzLnNlbGVjdG9yID0gaW5pdC5zZWxlY3RvcjtcclxuXHRcdHRoaXMuY291bnRTdGFycyA9IGluaXQuY291bnRTdGFycyArIDE7XHJcblx0XHR0aGlzLmFjdGl2ZVN0YXIgPSAoaW5pdC5hY3RpdmVTdGFyID49IDAgJiYgaW5pdC5hY3RpdmVTdGFyIDw9IGluaXQuY291bnRTdGFycykgPyBpbml0LmFjdGl2ZVN0YXIgOiAwO1xyXG5cdFx0Ly8gLS0tIFxyXG5cdFx0dGhpcy5yZW5kZXIoKTtcclxuXHR9XHJcblx0XHJcblx0X19uZXh0KGYpIHtcclxuXHRcdGNvbnN0IGFjdGl2ZUNsYXNzID0gKGYgPT09IHRoaXMuYWN0aXZlU3RhcikgPyAnYWN0aXZlJyA6ICcnO1xyXG5cdFx0Y29uc3QgZWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzcGFuJyk7XHJcblx0XHRlbC5jbGFzc0xpc3QgPSBgJHt0aGlzLnByZWZpeH1fX3N0YXJzLWVsICR7dGhpcy5wcmVmaXh9X19zdmcgJHt0aGlzLnByZWZpeH1fX3N2Zy0tc3RhciAke2FjdGl2ZUNsYXNzfWA7XHJcblx0XHRlbC5pbm5lckhUTUwgPSAnPHN2ZyByb2xlPVwicGljdHVyZS1zdmdcIiBjbGFzcz1cImdseXBoc19fc3RhclwiPjx1c2UgeGxpbms6aHJlZj1cIiNpZC1nbHlwaHMtc3RhclwiPjwvdXNlPjwvc3ZnPic7XHJcblx0XHR0aGlzLnNlbGVjdG9yLmFwcGVuZENoaWxkKGVsKTtcclxuXHR9XHJcblxyXG5cdHJlbmRlcigpIHtcclxuXHRcdEFycmF5LmZyb20oWy4uLkFycmF5KHRoaXMuY291bnRTdGFycykua2V5cygpXSwgZiA9PiB0aGlzLl9fbmV4dChmKSk7XHJcblx0fVxyXG59IiwiaW1wb3J0ICRyZW5kZXIgZnJvbSAnLi9yZW5kZXInO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgJGluZGV4IHtcclxuXHRjb25zdHJ1Y3Rvcihpbml0KSB7XHJcblx0XHQoe1xyXG5cdFx0XHRzZWxlY3RvcjogdGhpcy5zZWxlY3RvcixcclxuXHRcdFx0ZGVsZWdhdGU6IHRoaXMuZGVsZWdhdGUsXHJcblx0XHRcdHByZWZpeDogdGhpcy5wcmVmaXgsXHJcblx0XHRcdHByb2R1Y3RQcmVmaXg6IHRoaXMucHJvZHVjdFByZWZpeCxcclxuXHRcdFx0cHJvZHVjdFVybDogdGhpcy5wcm9kdWN0VXJsXHJcblx0XHR9ID0gaW5pdCk7XHJcblx0XHRcclxuXHRcdC8vIC0tLSBFUlJPUiAtLS1cclxuXHRcdHRoaXMuZXJyID0gdW5kZWZpbmVkO1xyXG5cdFx0dGhpcy53YXJuID0gbmV4dCA9PiBjb25zb2xlLmVycm9yKCdbU1lTVEVNXTonLCBuZXh0KTtcclxuXHRcdGlmICghdGhpcy5zZWxlY3RvcikgcmV0dXJuIHRoaXMud2Fybihgc2VsZWN0b3IgaXMgJHt0aGlzLmVycn1gKTtcclxuXHJcblx0XHQvLyAtLS0gVkFSSUFCTEVTIC0tLVxyXG5cdFx0dGhpcy5hcnJDb2xsZWN0aW9uID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCh0aGlzLnNlbGVjdG9yKTtcclxuXHR9XHJcblxyXG5cdF9fdmFsaWQocHJvcCwgbmV4dHByb3AsIGluZGV4KSB7XHJcblx0XHRyZXR1cm4gKHByb3BbaW5kZXhdICYmIHByb3BbaW5kZXhdW25leHRwcm9wXSkgPyBwcm9wW2luZGV4XVtuZXh0cHJvcF0gOiB0aGlzLmVycjtcclxuXHR9XHJcblxyXG5cdF9fcmVuZGVyKGYsIGluZGV4KSB7XHJcblx0XHR0aGlzLnJlbmRlciA9IG5ldyAkcmVuZGVyKHtcclxuXHRcdFx0cHJlZml4OiB0aGlzLnByZWZpeCxcclxuXHRcdFx0c2VsZWN0b3I6IGYsXHJcblx0XHRcdHByb2R1Y3RQcmVmaXg6IHRoaXMucHJvZHVjdFByZWZpeCxcclxuXHRcdFx0cHJvZHVjdFVybDogdGhpcy5fX3ZhbGlkKHRoaXMucHJvZHVjdFVybCwgJ2ltZ1VybCcsIGluZGV4KVxyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxuXHRydW4oKSB7XHJcblx0XHRBcnJheS5mcm9tKHRoaXMuYXJyQ29sbGVjdGlvbiwgKGYsIGluZGV4KSA9PiB0aGlzLl9fcmVuZGVyKGYsIGluZGV4KSk7XHJcblx0fVxyXG59XHJcbiIsImV4cG9ydCBkZWZhdWx0IGNsYXNzICRyZW5kZXIge1xyXG5cdGNvbnN0cnVjdG9yKGluaXQpIHtcclxuXHRcdCh7XHJcblx0XHRcdHByZWZpeDogdGhpcy5wcmVmaXgsXHJcblx0XHRcdHNlbGVjdG9yOiB0aGlzLnNlbGVjdG9yLFxyXG5cdFx0XHRwcm9kdWN0UHJlZml4OiB0aGlzLnByb2R1Y3RQcmVmaXgsXHJcblx0XHRcdHByb2R1Y3RVcmw6IHRoaXMucHJvZHVjdFVybFxyXG5cdFx0fSA9IGluaXQpO1xyXG5cclxuXHJcblx0XHQvLyAtLS0gRVJST1IgLS0tXHJcblx0XHR0aGlzLmVyciA9IHVuZGVmaW5lZDtcclxuXHRcdHRoaXMud2FybiA9IG5leHQgPT4gY29uc29sZS5lcnJvcignW1NZU1RFTV06JywgbmV4dCk7XHJcblx0XHRpZiAoIXRoaXMuc2VsZWN0b3IpIHJldHVybiB0aGlzLndhcm4oYHNlbGVjdG9yIGlzICR7dGhpcy5lcnJ9YCk7XHJcblxyXG5cdFx0Ly8gLS0tIFZBUklBQkxFUyAtLS1cclxuXHRcdHRoaXMuZGVmYXVsdEZha2UgPSAnaHR0cHM6Ly9hcGkuZm5rci5uZXQvdGVzdGltZy8yNjB4MTQ1L2VlZS9jY2MvP3RleHQ9aW1nJztcclxuXHRcdHRoaXMuc2VsZWN0UGljdHVyZSA9IHRoaXMucHJvZHVjdFVybCB8fCB0aGlzLmRlZmF1bHRGYWtlO1xyXG5cclxuXHRcdGlmICghdGhpcy5wcm9kdWN0VXJsIHx8ICF0aGlzLnByb2R1Y3RQcmVmaXgpIHRoaXMucmVzdWx0ID0gdGhpcy5kZWZhdWx0RmFrZTtcclxuXHRcdGVsc2UgdGhpcy5yZXN1bHQgPSB0aGlzLnByb2R1Y3RQcmVmaXggKyB0aGlzLnByb2R1Y3RVcmw7XHJcblx0XHQvLyAtLS1cclxuXHRcdHRoaXMucmVuZGVyKCk7XHJcblx0fVxyXG5cclxuXHRyZW5kZXIoKSB7XHJcblx0XHRjb25zdCBlbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3BpY3R1cmUnKTtcclxuXHRcdGVsLmlubmVySFRNTCA9IGBcclxuXHRcdFx0PHNvdXJjZSBzcmNzZXQ9XCIke3RoaXMucmVzdWx0fVwiIG1lZGlhPVwiKG1pbi13aWR0aDogMzIwcHgpXCI+XHJcblx0XHRcdDxpbWcgc3JjPVwiJHt0aGlzLnJlc3VsdH1cIiBhbHQ9XCJQaWN0dXJlXCIgdGl0bGU9XCJQaWN0dXJlXCI+XHJcblx0XHRgO1xyXG5cdFx0dGhpcy5zZWxlY3Rvci5hcHBlbmRDaGlsZChlbCk7XHJcblx0fVxyXG59XHJcbiIsImltcG9ydCAkcmVuZGVyIGZyb20gJy4vcmVuZGVyJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzICRpbmRleCB7XHJcblx0Y29uc3RydWN0b3IoaW5pdCkge1xyXG5cdFx0KHtcclxuXHRcdFx0c2VsZWN0b3I6IHRoaXMuc2VsZWN0b3IsXHJcblx0XHRcdGRlbGVnYXRlOiB0aGlzLmRlbGVnYXRlLFxyXG5cdFx0XHRwcmVmaXg6IHRoaXMucHJlZml4LFxyXG5cdFx0XHRjYXJkc0xlbmd0aDogdGhpcy5jYXJkc0xlbmd0aCxcclxuXHRcdFx0cHJvZHVjdFVybDogdGhpcy5wcm9kdWN0VXJsLFxyXG5cdFx0XHRzZWxlY3RvckpTOiB0aGlzLnNlbGVjdG9ySlNcclxuXHRcdH0gPSBpbml0KTtcclxuXHJcblx0XHQvLyAtLS0gRVJST1IgLS0tXHJcblx0XHR0aGlzLmVyciA9IHVuZGVmaW5lZDtcclxuXHRcdHRoaXMud2FybiA9IG5leHQgPT4gY29uc29sZS5lcnJvcignW1NZU1RFTV06JywgbmV4dCk7XHJcblx0XHRpZiAoIXRoaXMuZGVsZWdhdGUpIHJldHVybiB0aGlzLndhcm4oYGRlbGVnYXRlIGlzICR7dGhpcy5lcnJ9YCk7XHJcblxyXG5cdFx0Ly8gLS0tIFZBUklBQkxFUyAtLS1cclxuXHRcdHRoaXMucVNlbGVjdG9yID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0aGlzLmRlbGVnYXRlKTtcclxuXHR9XHJcblxyXG5cdF9fdmFsaWQocHJvcCwgZWwsIGluZGV4KSB7XHJcblx0XHRyZXR1cm4gKHByb3BbaW5kZXhdICYmIHByb3BbaW5kZXhdW2VsXSkgPyBwcm9wW2luZGV4XVtlbF0gOiB0aGlzLmVycjtcclxuXHR9XHJcblxyXG5cdF9fcmVuZGVyKGYpIHtcclxuXHRcdHRoaXMucmVuZGVyID0gbmV3ICRyZW5kZXIoe1xyXG5cdFx0XHRzZWxlY3RvcjogdGhpcy5kZWxlZ2F0ZSxcclxuXHRcdFx0cHJlZml4OiB0aGlzLnByZWZpeCxcclxuXHRcdFx0c2VsZWN0b3JKUzogdGhpcy5zZWxlY3RvckpTLFxyXG5cdFx0XHRzdG9ja0lEOiB0aGlzLl9fdmFsaWQodGhpcy5wcm9kdWN0VXJsLCAnaWQnLCBmKSxcclxuXHRcdFx0Y2FyZHNDb2RlOiB0aGlzLl9fdmFsaWQodGhpcy5wcm9kdWN0VXJsLCAnY29kZScsIGYpXHJcblx0XHR9KTtcclxuXHRcdHRoaXMuZW5kID0gdGhpcy5yZW5kZXIuZ3JpZCgpO1xyXG5cdFx0dGhpcy5yZW5kZXIubmV4dCh0aGlzLmVuZCk7XHJcblx0fVxyXG5cclxuXHRydW4oKSB7XHJcblx0XHRBcnJheS5mcm9tKFsuLi5BcnJheSh0aGlzLmNhcmRzTGVuZ3RoKS5rZXlzKCldLCBmID0+IHRoaXMuX19yZW5kZXIoZikpO1xyXG5cdH1cclxufVxyXG4iLCJleHBvcnQgZGVmYXVsdCBjbGFzcyAkcmVuZGVyIHtcclxuXHRjb25zdHJ1Y3Rvcihpbml0KSB7XHJcblx0XHQvLyAtLS0gVkFSSUFCTEVTIC0tLVxyXG5cdFx0dGhpcy5zZWxlY3RvciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoaW5pdC5zZWxlY3Rvcik7XHJcblx0XHR0aGlzLnByZWZpeCA9IGluaXQucHJlZml4O1xyXG5cdFx0dGhpcy5jYXJkc0NvZGUgPSBpbml0LmNhcmRzQ29kZTtcclxuXHRcdHRoaXMuYXJyTmFtZSA9IGluaXQuYXJyTmFtZTtcclxuXHRcdHRoaXMuc2VsZWN0b3JKUyA9IGluaXQuc2VsZWN0b3JKUztcclxuXHRcdHRoaXMuc3RvY2tJRCA9IGluaXQuc3RvY2tJRDtcclxuXHRcdC8vIC0tLVxyXG5cdH1cclxuXHJcblx0Z3JpZCgpIHtcclxuXHRcdHJldHVybiBgXHJcblx0XHQ8ZGl2IGNsYXNzPVwiJHt0aGlzLnByZWZpeH1fX2dyb3VwICR7dGhpcy5wcmVmaXh9X19iZXR3ZWVuICR7dGhpcy5wcmVmaXh9X19jb2x1bW5cIj5cclxuXHRcdFx0PGRpdiBjbGFzcz1cIiR7dGhpcy5wcmVmaXh9X19pdGVtICR7dGhpcy5wcmVmaXh9X19zdGFyc1wiICR7dGhpcy5zZWxlY3RvckpTLnJhdGluZ30gZGF0YS1yYXRpbmc9XCIzXCI+XHJcblx0XHRcdDwvZGl2PlxyXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiJHt0aGlzLnByZWZpeH1fX2l0ZW0gJHt0aGlzLnByZWZpeH1fX2NvZGVcIj7QkNGA0YIuICR7dGhpcy5jYXJkc0NvZGV9PC9kaXY+XHJcblx0XHRcdDwvZGl2PlxyXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiJHt0aGlzLnByZWZpeH1fX2l0ZW0gJHt0aGlzLnByZWZpeH1fX3BpY3R1cmVcIiAke3RoaXMuc2VsZWN0b3JKUy52aWV3UGljdHVyZX0+PC9kaXY+XHJcblx0XHRcdDxkaXYgY2xhc3M9XCIke3RoaXMucHJlZml4fV9fZ3JvdXAgJHt0aGlzLnByZWZpeH1fX2luZm9cIiAke3RoaXMuc2VsZWN0b3JKUy5pblN0b2NrfT5cclxuXHRcdFxyXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiJHt0aGlzLnByZWZpeH1fX2dyb3VwICR7dGhpcy5wcmVmaXh9X19saXN0XCIgJHt0aGlzLnNlbGVjdG9ySlMubGlzdFByb3BlcnR5fT48L2Rpdj5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHRcdFxyXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiJHt0aGlzLnByZWZpeH1fX2dyb3VwICR7dGhpcy5wcmVmaXh9X19wcmljZVwiPlxyXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiJHt0aGlzLnByZWZpeH1fX2dyb3VwICR7dGhpcy5wcmVmaXh9X19wcmljZS1lbFwiPlxyXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiJHt0aGlzLnByZWZpeH1fX2l0ZW0gJHt0aGlzLnByZWZpeH1fX3ByaWNlLW1haW5cIj40OSA5OTkg0YDRg9CxLiA8L2Rpdj5cclxuXHRcdFx0PGRpdiBjbGFzcz1cIiR7dGhpcy5wcmVmaXh9X19pdGVtICR7dGhpcy5wcmVmaXh9X19wcmljZS1ib251c1wiPis0OTAg0LHQvtC90YPRgdC+0LI8L2Rpdj5cclxuXHRcdFx0PC9kaXY+XHJcblxyXG5cdFx0XHQ8ZGl2IGNsYXNzPVwiJHt0aGlzLnByZWZpeH1fX2dyb3VwICR7dGhpcy5wcmVmaXh9X19jb2x1bW4gJHt0aGlzLnByZWZpeH1fX2JldHdlZW5cIj5cclxuXHRcdFx0PGEgdGl0bGU9XCJVUkxcIiByb2xlPVwibGlua1wiIGhyZWY9XCIjXCIgY2xhc3M9XCIke3RoaXMucHJlZml4fV9fYnV0dG9uICR7dGhpcy5wcmVmaXh9X19idXR0b24tLW1haW5cIj5cclxuXHRcdFx0PHNwYW4gY2xhc3M9XCIke3RoaXMucHJlZml4fV9fc3ZnICR7dGhpcy5wcmVmaXh9X19zdmctLWNhcnRcIj48c3ZnIHJvbGU9XCJwaWN0dXJlLXN2Z1wiIGNsYXNzPVwiZ2x5cGhzX19jYXJ0XCI+PHVzZSB4bGluazpocmVmPVwiI2lkLWdseXBocy1jYXJ0XCI+PC91c2U+PC9zdmc+PC9zcGFuPlxyXG5cdFx0XHQ8c3Bhbj7QmtGD0L/QuNGC0Yw8L3NwYW4+XHJcblx0XHRcdDwvYT5cclxuXHRcdFx0PGRpdiBjbGFzcz1cIiR7dGhpcy5wcmVmaXh9X19ncm91cCAke3RoaXMucHJlZml4fV9fZXh0cmFcIiA+XHJcblx0XHRcdDxkaXYgY2xhc3M9XCIke3RoaXMucHJlZml4fV9fZ3JvdXAgJHt0aGlzLnByZWZpeH1fX2Zhdm9yaXRlc1wiICR7dGhpcy5zZWxlY3RvckpTLmZhdm9yaXRlc30+PC9kaXY+XHJcblxyXG5cdFx0XHQ8c3BhbiBjbGFzcz1cIiR7dGhpcy5wcmVmaXh9X19zdmcgJHt0aGlzLnByZWZpeH1fX3JlcGFpblwiPjxzdmcgcm9sZT1cInBpY3R1cmUtc3ZnXCIgY2xhc3M9XCJnbHlwaHNfX2NvbXBhcmlzb25cIj5cclxuXHRcdFx0PHVzZSB4bGluazpocmVmPVwiI2lkLWdseXBocy1jb21wYXJpc29uXCI+PC91c2U+XHJcblx0XHRcdDwvc3ZnPjwvc3Bhbj5cclxuXHJcblx0XHRcdDwvZGl2PlxyXG5cdFx0XHQ8L2Rpdj5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHRcdGA7XHJcblx0fVxyXG5cclxuXHRuZXh0KG15QXJyYXkpIHtcclxuXHRcdGNvbnN0IGRpdiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ0RJVicpO1xyXG5cdFx0ZGl2LmNsYXNzTGlzdCA9IGAke3RoaXMucHJlZml4fSAke3RoaXMucHJlZml4fV9fYm94YDtcclxuXHRcdGRpdi5kYXRhc2V0LnN0b2NrSWQgPSBgJHt0aGlzLnN0b2NrSUR9YDtcclxuXHRcdGRpdi5pbm5lckhUTUwgPSBteUFycmF5O1xyXG5cclxuXHRcdHRoaXMuc2VsZWN0b3IuYXBwZW5kQ2hpbGQoZGl2KTtcclxuXHR9XHJcbn1cclxuIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQXJCQTtBQTRCQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMzREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBaEJBO0FBb0JBO0FBQ0E7QUFDQTtBQUdBO0FBRUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFVQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN2SUE7QUFDQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQURBO0FBTUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBRUE7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFKQTtBQU9BO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUxBO0FBT0E7QUFBQTtBQUNBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoSEE7OztBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQVFBO0FBRUE7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNyQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM0NBOzs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBRUE7QUFDQTtBQVFBO0FBVEE7QUFtQkE7QUFDQTtBQUNBO0FBSUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM1Q0E7QUFDQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzFDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBRUE7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdENBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBTUE7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUZBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN0RUE7OztBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNyQkE7QUFDQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3BDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBSUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeENBOzs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7OztBQUNBO0FBQ0E7QUFrQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBOzs7Ozs7Ozs7O0EiLCJzb3VyY2VSb290IjoiIn0=